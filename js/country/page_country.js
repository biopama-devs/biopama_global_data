 //test URL
 //https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_all_inds?format=json&c_un_m49=516
 
 /* 

 all cards and data are found in drupalSettings.dataCards
 
 console.log(drupalSettings.dataCards ); //to find your data

 */
 //console.log(drupalSettings.dataCards );

var countryChanged = 1;

function updateCountryPageURL(iso2){
	const state = { country: iso2 };
	history.pushState(state, '', '/ct/country/' + iso2); 
	window.dispatchEvent(new Event('popstate'));
}

(function ($, Drupal) {
	
	var pathArray = window.location.pathname.split('/');
	var pathIsoCode = pathArray[pathArray.length - 1]; //gets the last thing in the URL (should be a country code)
	//var selSettings.iso2 = pathIsoCode.toUpperCase(); //coverts it to upper case as the REST needs upper case
	selSettings.iso2 = pathIsoCode.toUpperCase(); //coverts it to upper case as the REST needs upper case

	var allCountryInds = []; //Country Indicators
	var allCountryPaStats = []; //Protected Area Indicators
	var countryProtStats;
	

	var popupOptions = {
		countryLink: '',
		paLink: '/ct/pa/',
	}
	$().addMapLayerInteraction(mymap, popupOptions);
	 
	getIndicatorData(selSettings.iso2); //We run the big indicator call right away and try to get all our data in the background. 

	function getIndicatorData(isoCode){
		
		var allCountryIndicatorsURL = DopaBaseUrl+'get_dopa_country_all_inds?format=json&country_code='+isoCode;
		var allCountryPaStatsURL = DopaBaseUrl+'get_bp_country_pa_stats?format=json&country_code='+isoCode;
		$('.indicatorProccessed').removeClass('indicatorProccessed');

		$.when(
			$.getJSON(allCountryIndicatorsURL,function(d){
				allCountryInds = d['records'][0];
				selSettings.iso2 = allCountryInds.iso2;
				$('.scope-name').text(selSettings.countryName);
				$('#wdpa-vt-link').html("<a href='https://wdpaversiontracker.biopama.org/?iso3=" + selSettings.iso3 + "&from=mar_2022&to=nov_2022' target='_blank'>Click here to see the wdpa change over time for " + selSettings.countryName + "</a>");
				$('#country-flag').attr("src", '/sites/default/files/country_flags/' + selSettings.iso2.toLowerCase() + '.png');
				document.title = 'Country Data | ' + selSettings.countryName;
			}),
			$.getJSON(allCountryPaStatsURL,function(d){
				allCountryPaStats = d['records']; 
			})
		).then(function() {
			$( ".wrapper-data-card.show" ).each(function() {  
				$( this ).trigger("shown.bs.collapse"); 
			});
			$( "#collapsePanorama.show" ).each(function() {  //to update the panorama card specifically
				$( this ).trigger("shown.bs.collapse"); 
			});

			//populateTopInfo();

			// var url = "https://estation.jrc.ec.europa.eu/eStation2/analysis/gettimeseriesprofile";

			// var xhr = new XMLHttpRequest();
			// xhr.open("POST", url);

			// xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

			// xhr.onreadystatechange = function () {
			// if (xhr.readyState === 4) {
			// 	//console.log(xhr.status);
			// 	//console.log(xhr.responseText);
			// }};

			// var data = "productcode=vgt-ndvi&version=vgt-pv-olci&subproductcode=linearx2diff-linearx2&mapsetcode=SPOTV-Africa-1km&yearsToCompare=%5B2022%5D&WKT=POLYGON((24.06593142292029%20-2.420308086552713%2C24.07417116901404%20-2.420308086552713%2C24.07417116901404%20-2.4305985419638585%2C24.06593142292029%20-2.4305985419638585%2C24.06593142292029%20-2.420308086552713))";

			// xhr.send(data);

			if ($(".ctt-card-title[aria-expanded='true']").length) {
				getRestResults();
			}
		});
	}

	mymap.on('load', function () {
		if (countryChanged == 1){
			countryChanged = 0;
			updateCountry();
			updateCountryPageURL(selSettings.iso2);
		}
	});

	mymap.on('moveend', function () {
		//this is really just for when the page first loads
		if (countryChanged == 1){	
			countryChanged = 0;
			updateCountryPageURL(selSettings.iso2);
		}
	});


	function buildEstationArgs(args){ 
		var returnArgs = '';
		returnArgs += 'productcode=' + args.productcode + '&';
		returnArgs += 'version=' + args.version + '&';
		returnArgs += 'subproductcode=' + args.subproductcode + '&';
		returnArgs += 'mapsetcode=' + args.mapsetcode + '&';
		returnArgs += 'yearsToCompare=' + args.yearsToCompare + '&';
		returnArgs += 'WKT=' + args.WKT;
		return returnArgs;
	}

	function populateTopInfo(){ 
		/**
		 * 
		 * The data at the top of the country page section goes here.
		 * 
		 */
		//
		var ProtectedPlanetUrl = "https://api.protectedplanet.net/v3/countries/" + selSettings.iso3 + "?token=fd53a539938fa8b7d20166ef14b219b4";
		$.getJSON(ProtectedPlanetUrl,function(d){
			countryProtStats = d.country;

			for(propertyName in countryProtStats.statistics){
				if(typeof countryProtStats.statistics[propertyName] === 'number'){
					countryProtStats.statistics[propertyName] = countryProtStats.statistics[propertyName].toFixed(2);
				}
			}

			$(".total-pa-num").text(countryProtStats.pas_count);
			// $(".terrestrial-pa-num").text(countryProtStats.pa_count_polygons_terrestrial + countryProtStats.pa_count_points_terrestrial);
			// $(".marine-pa-num").text(countryProtStats.pa_count_polygons_marine + countryProtStats.pa_count_points_marine || '0');
			// $(".coastal-pa-num").text(countryProtStats.pa_count_polygons_coastal + countryProtStats.pa_count_points_coastal || '0');
			$(".national-pa-num").text(countryProtStats.pas_national_count  || '0');
			$(".international-pa-num").text(countryProtStats.pas_international_count || '0');
			$(".regional-pa-num").text(countryProtStats.pas_regional_count || '0');

			var terProc = (Number(countryProtStats.statistics.percentage_pa_land_cover)).toFixed(2);
			var marProc = (Number(countryProtStats.statistics.percentage_pa_marine_cover)).toFixed(2);

			var indicatorTitle = "Terrestrial protection"; 
			var tableChartColors = biopamaGlobal.chart.colors.twoTerrestrial;
			var chartData = {
				colors: tableChartColors,
				singleVal: true,
				toolbox: { show: false},
				tooltip: { show: false},
				series: [{
					name: indicatorTitle,
					type: 'pie',
					radius: ['60%', '70%'],
					avoidLabelOverlap: true,
					data: [
						{value: terProc, name: terProc + '%'},
						{value: (100 - terProc).toFixed(2)},
					] 
				}]
			}
			$().createNoAxisChart("#ter-prot-chart" , chartData); 

			indicatorTitle = "Marine protection"; 
			tableChartColors = biopamaGlobal.chart.colors.twoMarine;
			var chartData = {
				colors: tableChartColors,
				singleVal: true,
				toolbox: { show: false},
				tooltip: { show: false},
				series: [{
					name: indicatorTitle,
					type: 'pie',
					radius: ['60%', '70%'],
					avoidLabelOverlap: true,
					data: [
						{value: marProc, name: marProc + '%'},
						{value: (100 - marProc).toFixed(2)},
					] 
				}]
			}
			$().createNoAxisChart("#mar-prot-chart" , chartData); 

		});

		indicatorTitle = "Terrestrial Connectivity"; 
		tableChartColors = [biopamaPAColors.protConn, biopamaPAColors.noColor];
		var chartData = {
			colors: tableChartColors,
			singleVal: true,
			toolbox: { show: false},
			tooltip: { show: false},
			series: [{
				name: indicatorTitle,
				type: 'pie',
				radius: ['60%', '70%'],
				avoidLabelOverlap: true,
				data: [
					{value: allCountryInds.protconn, name: allCountryInds.protconn + '%'},
					{value: (100 - allCountryInds.protconn).toFixed(2)},
				] 
			}]
		}
		$().createNoAxisChart("#ter-conn-chart" , chartData); 
		
	};

	window.addEventListener('popstate', function (event) {
		var pathArray = window.location.pathname.split('/');
		var pathIsoCode = pathArray[pathArray.length - 1]; //gets the last thing in the URL (should be a country code)
		selSettings.iso2 = pathIsoCode.toUpperCase(); //coverts it to upper case as the REST needs upper case
		getIndicatorData(selSettings.iso2);
		
	}, false);
    Drupal.behaviors.biopamaDopa = {
		attach: function (context, settings) {
		    $(window).once().on('load scroll', function () {
				var nationalSelect = $().createNationalSelect();
				$("#country-search").append( nationalSelect );
				$('.biopama-country-selector').on('change', function() {
					selSettings.iso2 = $(this).val();
					countryChanged = 1;
				});
				
				$(".biopama-country-selector").chosen({
					disable_search_threshold: 5,
					width: "100%"
				}); 
				$(".biopama-country-selector").val(selSettings.iso2).trigger("chosen:updated");
				
				$(".biopama-country-selector").chosen().change(function(){
					selSettings.iso2 = $(this).val();
					var iso2 = $(this).val().toLowerCase();
					updateCountry();
					updateCountryPageURL(iso2);
				});
				
				$( ".report-icon" ).click(function( event ) {
				  	$(this).toggleClass("report-added");
					var reportCardID = $(this).closest('.card-text').prev().attr("data-bs-target");
					var reportCardPart = 'Chart';
					if ( $(this).next().hasClass('wrapper-data-table') ) {
						reportCardPart = 'Table';
					}
					var indicator = reportCardID.replace("#collapse", "");
					if ($(this).hasClass("report-added")){
						$().addIndicatorToReport(indicator, reportCardPart);
					} else {
						$().removeIndicatorFromReport(indicator, reportCardPart); 
					}
				});
				
				// #### Card Start  - we go through every card and attach the functions needed to generate the content
				
				//! //! ## Start Profile ##
				$('#collapseProfile').on('shown.bs.collapse', function () {
					console.log(selSettings);
					//this card is a custom one to update the URLS's of all the links
					$(this).find("a.token-link").each(function( i ) {
						var linkHref = $(this).attr("href");
						console.log(linkHref);
						linkHref = $().biopamaReplaceTokens(linkHref, selSettings);
						$(this).attr("href", linkHref)
					});
				});
				//! End Profile

				//! //! ## Start Happening Now Fires ##
				$('#collapseFires').on('shown.bs.collapse', function () {
					var cardTwigName = 'Fires'; //Set this once here as so there are no mistakes below.
					$( "div.fires_1d" ).tooltip({
							title: moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_7d" ).tooltip({
							title: moment().subtract(7, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_30d" ).tooltip({
							title: moment().subtract(30, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_90d" ).tooltip({
							title: moment().subtract(90, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					var layerArgs = moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
					//$( "button[data-card-id='"+cardTwigName+"'].active" ).removeClass("active");
					//$( "button[data-layer-id='fires_1d']" ).addClass("active"); //forcing fires at 1 day to be active since the args are needed and this is the way
					$().addCardMapLayer(cardTwigName, layerArgs, "fires_1d"); 
					$( "button.layer-button#layer-button-fires_1d" ).click(function( event ) {
						var layerArgs = moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_1d"); 
					});
					$( "button.layer-button#layer-button-fires_7d" ).click(function( event ) {
						var layerArgs = moment().subtract(7, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_7d"); 
					});
					$( "button.layer-button#layer-button-fires_30d" ).click(function( event ) {
						var layerArgs = moment().subtract(30, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_30d"); 
					});
					$( "button.layer-button#layer-button-fires_90d" ).click(function( event ) {
						var layerArgs = moment().subtract(90, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_90d");  
					});
				});
				//! //! ## End Happening Now Fires ##
				
				//! //! ## Start Happening Now Floods ##
				$('#collapseFloods').on('shown.bs.collapse', function () {
					var cardTwigName = 'Floods'; //Set this once here as so there are no mistakes below.
					var layerArgs = moment().format('YYYY-MM-DD');
					$().addCardMapLayer(cardTwigName, layerArgs, "floods");
				});
				//! //! ## End Happening Now Floods ##

				//! //! ## Start Happening Now Droughts ##
				$('#collapseDroughts').on('shown.bs.collapse', function () {
					var cardTwigName = 'Droughts'; //Set this once here as so there are no mistakes below.
					var edoDay = moment().format('DD');
					var edoMonth = moment().format('MM') - 1;
					if(edoDay <=10) {edoDay = '01'}else if(edoDay >=11 && edoDay <= 20){edoDay = '11'}else if(edoDay >=21 ){edoDay = '21'}else{}
					var layerArgs = 'SELECTED_YEAR=' + moment().format('YYYY') + '&SELECTED_MONTH=' + edoMonth + '&SELECTED_TENDAYS='+edoDay;
					$().addCardMapLayer(cardTwigName, layerArgs); 
				});
				//! //! ## End Happening Now Droughts ##

				//! //! ## Start Happening Now Sea Surface Temperature Anomalies ##
				$('#collapseSST').on('shown.bs.collapse', function () {
					var cardTwigName = 'SST'; //Set this once here as so there are no mistakes below.
				});
				//! //! ## End Happening Now Sea Surface Temperature Anomalies ##

				//! //! ## Start Happening Now Coral Bleaching Heat Stress Alerts ##
				$('#collapseCoralBleaching').on('shown.bs.collapse', function () {
					var cardTwigName = 'CoralBleaching'; //Set this once here as so there are no mistakes below.
				});
				//! //! ## End Happening Now Coral Bleaching Heat Stress Alerts ##

				//! //! ## Start Climate Elevation Profile ##
				$('#collapseElevationProfile').on('shown.bs.collapse', function () {
					var cardTwigName = 'ElevationProfile'; //Set this once here as so there are no mistakes below.
					 
					var indicatorTitle = "Virtual elevation profile for " + selSettings.countryName; 
					var tableChartColors = ['#65a70c'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxis: {
							name: "Elevation (m) above sea level", 
						},
						xAxis: {
							type: 'category',
							data: [ 'Min.' , 'Max.' ]
						},
						series: [{
						  name: 'Elevation',
						  data: [allCountryInds.elev_min, allCountryInds.elev_max],
						  type: 'line',
						  areaStyle: {
							color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
								offset: 0,
								color: '#65a70c'
							}, {
								offset: 1,
								color: '#ffe'
							}])
						  },
						  label: {
							  show: true,
							  formatter: function(d) { 
								return d.name + ' ' + d.data + 'm';
							  }
						  },
						  markLine: {
							  data: [{
								type: "average",
								name: 'Average',
							  }],
							  label: {
								show: true,
								position: "insideEndTop",
								formatter: '{b}: {c}m',
							  },
							  lineStyle: {
								  color: '#65a70c',
							  }
						  },
						}]
					}
					$().createXYAxisChart(cardTwigName , chartData);  
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.elev_min, allCountryInds.elev_mean, allCountryInds.elev_max]]; 
						var columnData = [
							{ title: "Min. (m)" },
            				{ title: "Mean (m)" },
            				{ title: "Max. (m)" },
						];
						var columnSettings = [{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": "_all" }];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! //! ## End Climate Elevation Profile ##
				 
				//! Start Ecosystems Terrestrial ecoregions and Marine ecoregions  ##
				$('#collapseTerrestrialEcoregions, #collapseMarineEcoregions').on('shown.bs.collapse', function () {
					//$().checkForDataTables("TerrestrialEcoregions"); //marine and terrestrial are linked, if one is there we can remove both, so just need to check one. Same for creating below
					//Datatable Complex
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader('#data-table-TerrestrialEcoregions');
					$().insertBiopamaLoader('#data-table-MarineEcoregions');

					var countryEcroregionUrl = DopaBaseUrl+'get_dopa_country_ecoregion_all_inds?format=json&country_code='+selSettings.iso2;
					$.getJSON(countryEcroregionUrl,function(d){
						var terrestrialDataSet = [];
						var marineDataSet = [];
						$(d.records).each(function(i, data) {
							var newDataRow = [];
							newDataRow.push(data.ecoregion, data.country_eco_sqkm, data.country_eco_perc_ecoregion_tot, data.country_eco_prot_perc_country_eco, data.country_eco_prot_perc_ecoregion_prot, parseFloat( (data.country_eco_prot_sqkm / data.country_eco_sqkm) * 100 ).toFixed(2));
							if(data.is_marine){
								marineDataSet.push(newDataRow); 
							} else {
								terrestrialDataSet.push(newDataRow); 
							}
						});
						var columnData = [
							{title: "Name"},
							{title: "Area (km\u00B2)"},
							{title: "% of ecoregion in country"},
							{title: "% of ecoregion protected in country"},
							{title: "% country contribution to global protection"},
							{title: "% of ecoregion protected worldwide"},
						];
						var columnSettings = [{ 
							"targets": [ 2, 3, 4 ],
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData == null) cellData = 0;
								$(cell).html(cellData+'<div class="table-indicator-bar aichinoc'+(colIndex-1)+'"></div>');
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						},{ 
							"targets": [ 5 ],
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 17){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 10 && cellData <  17) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 5 && cellData <  10) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  5) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  2) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(17-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						}];
						var columnSettingsMarine = [{ 
							"targets": [ 2, 3, 4 ],
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData == null) cellData = 0;
								$(cell).html(cellData+'<div class="table-indicator-bar aichinoc'+(colIndex-1)+'"></div>');
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						},{ 
							"targets": [ 5 ],
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 17){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 10 && cellData <  8) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 5 && cellData <  4) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  2) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  1) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(10-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						}];
						var terrestrialTableData = {
							title: "Terrestrial Ecoregions for " + selSettings.countryName,
							columns: columnData,
							columnDefs: columnSettings,
							data: terrestrialDataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						var marineTableData = {
							title: "Terrestrial Ecoregions for " + selSettings.countryName,
							columns: columnData,
							columnDefs: columnSettingsMarine,
							data: marineDataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('#data-table-TerrestrialEcoregions', terrestrialTableData);
						$().createDataTable('#data-table-MarineEcoregions', marineTableData);
					});
				});
				//! End Ecosystems Terrestrial ecoregions and Marine ecoregions  ##
				 
				//! ## Start Ecosystems Inland Surface Water  ##
				$('#collapseInlandSurfaceWater').on('shown.bs.collapse', function () {
					var cardTwigName = 'InlandSurfaceWater'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Area of Permanent and Seasonal Water for " + selSettings.countryName; 
					var tableChartColors = ['#1710f7','#c184c1']; //this will be used to coordinate the coloring of BOTH table and chart
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxis: {
							name: "Area (km\u00B2)", 
						},
						xAxis: {
							type: 'category',
							data: ['1984','2020']
						},
						series: [{
						  name: 'Permanent Water',
						  data: [ parseFloat(allCountryInds.water_p_now_sqkm)-(parseFloat(allCountryInds.water_p_netchange_sqkm)) , allCountryInds.water_p_now_sqkm],
						  type: 'bar',
						},{
						  name: 'Seasonal  Water',
						  data: [ parseFloat(allCountryInds.water_s_now_sqkm)-(parseFloat(allCountryInds.water_s_netchange_sqkm)) , allCountryInds.water_s_now_sqkm],
						  type: 'bar',
						}]
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.water_p_now_sqkm, allCountryInds.water_s_now_sqkm, allCountryInds.water_p_netchange_sqkm, allCountryInds.water_s_netchange_sqkm, allCountryInds.water_p_netchange_perc_first_epoch, allCountryInds.water_s_netchange_perc_first_epoch]]; 
						var columnData = [
							{ title: "Area (km\u00B2) of permanent surface water (2020)" },
            				{ title: "Area (km\u00B2) of seasonal inland water (2020)" },
            				{ title: "Net change (km\u00B2) of permanent surface water (1984 - 2020)" },
            				{ title: "Net change (km\u00B2) of seasonal inland water " },
							{ title: "Net change (%) of permanent surface water (1984 - 2020)" },
							{ title: "Net change (%) in surface area of seasonal inland water " },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystems Inland Surface Water  ##
				
				//! ## Start Ecosystems Forest cover  ##
				$('#collapseForestCover').on('shown.bs.collapse', function () {

					var thisUrl = biopamaApiUrl+'/api/forest/function/api_country_tree_cover_loss_prot_unprot_2001_2020/isoa3_id='+selSettings.iso3;
					$.getJSON(thisUrl,function(d){
						data = d[0];
						var cardTwigName = 'ForestCover'; //Set this once here as so there are no mistakes below.
					
						var indicatorTitle = "Forest loss in protected and unprotected areas for " + selSettings.countryName; 
						var tableChartColors = ['#ff0000','#ff6c6c'];
						//Chart
						var chartData = {
							colors: tableChartColors,
							title: indicatorTitle,
							yAxis: {
								name: "Percent", 
							},
							xAxis: {
								type: 'category',
								data: ['% Loss in protectred areas','% Loss in unprotectred areas']
							},
							series: [{
							  data: [{
								  value: data.percentage_loss_protected_areas	,
								itemStyle: {
									color: tableChartColors[0]
								  }
							  },{
								  value: data.percentage_loss_unprotected_areas,
								itemStyle: {
									color: tableChartColors[1]
								  }
							  }],
							  type: 'bar',
							}] 
						}
						
						$().createXYAxisChart(cardTwigName , chartData); 
						
						if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
							var dataSet = [[data.country_cover_protected_areas_2000, data.country_loss_protected_areas_2000_2021, data.percentage_loss_protected_areas, data.country_cover_unprotected_areas_2000, data.country_loss_unprotected_areas_2000_2021, data.percentage_loss_unprotected_areas]]; 
							var columnData = [
								{ title: "Forest cover protected 2000 (km\u00B2)" },
								{ title: "Loss in protected areas 2000-2021 (km\u00B2)" },
								{ title: "Percentage loss in protected areas (%)" },
								{ title: "Forest cover unprotected 2000 (km\u00B2)" },
								{ title: "Loss in unprotected areas 2000-2021 (km\u00B2)" },
								{ title: "Percentage loss in unprotected areas (%)" },
							];
							var columnSettings = [
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5 ] },
								{ "className": "dt-center", "targets": "_all"}
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						}
					});

				});
				//! ## End Ecosystems Forest cover  ##
				
				//! ## Start Ecosystems Land Degradation  ##
				$('#collapseLandDegradation').on('shown.bs.collapse', function () {
					var cardTwigName = 'LandDegradation'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Land degradation for " + selSettings.countryName; 
					var tableChartColors = ['#ab2828', '#ed7325', '#ffd954', '#b8d879', '#46a246','#c2c5cc'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle, 
						series: [{
							name: indicatorTitle,
							type: 'pie',
							radius: ['50%', '70%'],
							avoidLabelOverlap: true,
							data: [
								{value: allCountryInds.lpd_severe_sqkm, name: 'Persistent severe decline in productivity'},
								{value: allCountryInds.lpd_moderate_sqkm, name: 'Persistent moderate decline in productivity'},
								{value: allCountryInds.lpd_stable_stressed_sqkm, name: 'Stable, but stressed'},
								{value: allCountryInds.lpd_stable_sqkm, name: 'Stable Productivity'},
								{value: allCountryInds.lpd_increased_sqkm, name: 'Persistent increase in productivity'},
								{value: allCountryInds.lpd_null_sqkm, name: 'No biomass'},
							] 
						}]
					}
					
					$().createNoAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.lpd_null_sqkm, allCountryInds.lpd_severe_sqkm, allCountryInds.lpd_moderate_sqkm, allCountryInds.lpd_stable_stressed_sqkm, allCountryInds.lpd_stable_sqkm, allCountryInds.lpd_increased_sqkm]]; 
						var columnData = [
							{ title: "No biomass (km\u00B2)" },
            				{ title: "Persistent severe decline in productivity (km\u00B2)" },
            				{ title: "Persistent moderate decline in productivity (km\u00B2)" },
            				{ title: "Stable, but stressed; persistent strong inter-annual productivity variations (km\u00B2)" },
							{ title: "Stable Productivity (km\u00B2)" },
							{ title: "Persistent increase in productivity (km\u00B2)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-5", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-4", "targets": [ 5 ] },
						];
						var taiobleData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystems Land Degradation  ##
				
				//! ## Start Ecosystems Land Fragmentation  ##
				$('#collapseLandFragmentation').on('shown.bs.collapse', function () {
					var cardTwigName = 'LandFragmentation'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Natural land pattern fragmentation for " + selSettings.countryName + " (1995-2015)";
					var tableChartColors = ['#7fad41', '#878787', '#52781f', '#f1c200', '#e89717','#d56317'];
					
					//an object is used here to calculate the percentages.
					var landFragmentationData = {
						core: [allCountryInds.mspa_core_1995_sqkm, allCountryInds.mspa_core_2000_sqkm, allCountryInds.mspa_core_2005_sqkm, allCountryInds.mspa_core_2010_sqkm, allCountryInds.mspa_core_2015_sqkm],
						non_natural: [allCountryInds.mspa_non_natural_1995_sqkm, allCountryInds.mspa_non_natural_2000_sqkm, allCountryInds.mspa_non_natural_2005_sqkm, allCountryInds.mspa_non_natural_2010_sqkm, allCountryInds.mspa_non_natural_2015_sqkm],
						edges: [allCountryInds.mspa_edge_1995_sqkm, allCountryInds.mspa_edge_2000_sqkm, allCountryInds.mspa_edge_2005_sqkm, allCountryInds.mspa_edge_2010_sqkm, allCountryInds.mspa_edge_2015_sqkm],
						perforation: [allCountryInds.mspa_core_perforation_1995_sqkm, allCountryInds.mspa_core_perforation_2000_sqkm, allCountryInds.mspa_core_perforation_2005_sqkm, allCountryInds.mspa_core_perforation_2010_sqkm, allCountryInds.mspa_core_perforation_2015_sqkm],
						islets: [allCountryInds.mspa_islet_1995_sqkm, allCountryInds.mspa_islet_2000_sqkm, allCountryInds.mspa_islet_2005_sqkm, allCountryInds.mspa_islet_2010_sqkm, allCountryInds.mspa_islets_2015_sqkm],
						linear: [allCountryInds.mspa_linear_1995_sqkm, allCountryInds.mspa_linear_2000_sqkm, allCountryInds.mspa_linear_2005_sqkm, allCountryInds.mspa_linear_2010_sqkm, allCountryInds.mspa_linear_2015_sqkm],
					};

					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxis: {
							name: "km\u00B2", 
						},
						xAxis: {
							type: 'category',
							data: ['1995','2000','2005','2010','2015']
						},
						series: [{
							name: 'Core',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.core,
						}, {
							name: 'Non Natural',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.non_natural,
						}, {
							name: 'Edge',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.edges,
						}, {
							name: 'Core Perforation',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.perforation,
						},{
							name: 'Islet',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.islets,
						}, {
							name: 'Linear',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.linear,
						}]
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [
							[1995, allCountryInds.mspa_core_1995_sqkm, allCountryInds.mspa_non_natural_1995_sqkm, allCountryInds.mspa_edge_1995_sqkm, allCountryInds.mspa_core_perforation_1995_sqkm, allCountryInds.mspa_islet_1995_sqkm, allCountryInds.mspa_linear_1995_sqkm],
							[2000, allCountryInds.mspa_core_2000_sqkm, allCountryInds.mspa_non_natural_2000_sqkm, allCountryInds.mspa_edge_2000_sqkm, allCountryInds.mspa_core_perforation_2000_sqkm, allCountryInds.mspa_islet_2000_sqkm, allCountryInds.mspa_linear_2000_sqkm],
							[2005, allCountryInds.mspa_core_2005_sqkm, allCountryInds.mspa_non_natural_2005_sqkm, allCountryInds.mspa_edge_2005_sqkm, allCountryInds.mspa_core_perforation_2005_sqkm, allCountryInds.mspa_islet_2005_sqkm, allCountryInds.mspa_linear_2005_sqkm],
							[2010, allCountryInds.mspa_core_2010_sqkm, allCountryInds.mspa_non_natural_2010_sqkm, allCountryInds.mspa_edge_2010_sqkm, allCountryInds.mspa_core_perforation_2010_sqkm, allCountryInds.mspa_islet_2010_sqkm, allCountryInds.mspa_linear_2010_sqkm],
							[2015, allCountryInds.mspa_core_2015_sqkm, allCountryInds.mspa_non_natural_2015_sqkm, allCountryInds.mspa_edge_2015_sqkm, allCountryInds.mspa_core_perforation_2015_sqkm, allCountryInds.mspa_islet_2015_sqkm, allCountryInds.mspa_linear_2015_sqkm]];  
						var columnData = [
							{ title: "Year" },
							{ title: "Core (km\u00B2)" },
            				{ title: "Non Natural (km\u00B2)" },
            				{ title: "Edge (km\u00B2)" },
            				{ title: "Core Perforation (km\u00B2)" },
							{ title: "Islet (km\u00B2)" },
							{ title: "Linear (km\u00B2)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-4", "targets": [ 5 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-5", "targets": [ 6 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystems Land Fragmentation  ##

				//! ## Start Ecosystem Services Below Ground Carbon  ##
				$('#collapseBelowGroundCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'BelowGroundCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Below ground carbon for " + selSettings.countryName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.bgb_min_c_mg_total, allCountryInds.bgb_mean_c_mg_total, allCountryInds.bgb_max_c_mg_total, (allCountryInds.bgb_tot_c_pg_total).toFixed(2)]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystem Services Below Ground Carbon  ##

				//! ## Start Ecosystem Services Soil Organic Carbon  ##
				$('#collapseSoilOrganicCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'SoilOrganicCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Soil organic carbon for " + selSettings.countryName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.gsoc_min_c_mg_total, allCountryInds.gsoc_mean_c_mg_total, allCountryInds.gsoc_max_c_mg_total, (allCountryInds.gsoc_tot_c_pg_total).toFixed(2)]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystem Services Soil Organic Carbon  ##

				//! ## Start Ecosystem Services Above ground carbon  ##
				$('#collapseAboveGroundCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'AboveGroundCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Above ground carbon for " + selSettings.countryName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.agb_min_c_mg_total, allCountryInds.agb_mean_c_mg_total, allCountryInds.agb_max_c_mg_total, allCountryInds.agb_tot_c_pg_total]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystem Services Above ground carbon  ##

				//! ## Start Ecosystem Services Little carbon  ##
				$('#collapseLitterCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'LitterCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Litter carbon for " + selSettings.countryName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.ltc_min_c_mg_total, allCountryInds.ltc_mean_c_mg_total, allCountryInds.ltc_max_c_mg_total, (allCountryInds.ltc_tot_c_pg_total).toFixed(2)]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystem Services Litter carbon  ##

				//! ## Start Ecosystem Services Dead Wood carbon  ##
				$('#collapseDeadWoodCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'DeadWoodCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Dead wood carbon for " + selSettings.countryName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.dwc_min_c_mg_total, allCountryInds.dwc_mean_c_mg_total, allCountryInds.dwc_max_c_mg_total, (allCountryInds.dwc_tot_c_pg_total).toFixed(2)]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystem Services Dead Wood carbon  ##

				//! ## Start Ecosystem Services Total carbon  ##
				$('#collapseTotalCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'TotalCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Total carbon for " + selSettings.countryName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.carbon_min_c_mg_total, allCountryInds.carbon_mean_c_mg_total, allCountryInds.carbon_max_c_mg_total, (allCountryInds.carbon_tot_c_pg_total).toFixed(2)]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Ecosystem Services Total carbon  ##

				//! ## Start Land Cover Copernicus Global Land Cover 2015  ##
				$('#collapseCopernicusGLC').on('shown.bs.collapse', function () {
					var cardTwigName = 'CopernicusGLC'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Copernicus Land Cover " + selSettings.countryName; 
					var copernicusLandCoverUrl = DopaBaseUrl + 'get_dopa_country_lc_copernicus?agg=0&sortfield=lc_land_perc_country_land&country_code='+selSettings.num;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(copernicusLandCoverUrl,function(d){
							var dataSet = [];
							var chartSeriesData = [];
							var tableChartColors = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(data.label, data.lc_land_perc_country_land, data.lc_land_prot_perc_country_land, data.lc_land_sqkm, data.lc_land_prot_sqkm, data.color);
								dataSet.push(newDataRow); 
								chartSeriesData.push({'name': data.label,'value': data.lc_land_sqkm}) 
								tableChartColors.push(data.color) 
							});
							tableChartColors.reverse();

							var chartData = {
								colors: tableChartColors,
								title: indicatorTitle, 
								series: [{
									type: 'treemap',
									data: chartSeriesData
								}]
							}
							$().createNoAxisChart(cardTwigName , chartData); 

							var columnData = [
								{ title: "Land Cover Class" },
								{ title: "% Covered" },
								{ title: "% Protected" },
								{ title: "Calculated Surface (km2)" },
								{ title: "Protected Surface (km2)" },
								{ title: "Color Map" },
							];
							var columnSettings = [
								{
									"targets": 5,
									"createdCell": function (td, cellData) {
										$(td).attr('style', 'background: '+ cellData +' !important; color: '+ cellData +';');
									},
								},
								{ "className": cardTwigName, "targets": [ "_all" ] }
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true,
								defaultSort: [[1, 'desc']],
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						});
					}
				});
				//! ## End Land Cover Copernicus Global Land Cover 2015  ##

				//! ## Start Land Cover ESA Land Cover change 1995-2015  ##
				$('#collapseEsaLC').on('shown.bs.collapse', function () {
					var cardTwigName = 'EsaLC'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Natural land pattern fragmentation in " + selSettings.countryName + " (1995-2020)";
					$.getJSON(DopaBaseUrl + "get_dopa_country_lcc_esa?format=json&sortfield=lcc_sqkm&country_code="+selSettings.num,
						function(response){

							let data = response.records.sort((a, b) => b.lcc_sqkm - a.lcc_sqkm);

							let dataKeys = {};
							let dataNodes = [];
							let dataLinks = [];
							data.forEach(d => {
								var yr1995 = d.lc1_1995.trim() + ' 1995';
								var yr2020 = d.lc1_2020.trim() + ' 2020';
								if(!dataKeys[d.lc1_1995]){
									dataKeys[d.lc1_1995] = true;
									dataNodes.push({
										name: yr1995,
										itemStyle: {
											color: getLcColor(yr1995),
										}
									});
								}

								if(!dataKeys[d.lc1_2020]){
									dataKeys[d.lc1_2020] = true;
									dataNodes.push({
										name: yr2020,
										itemStyle: {
											color: getLcColor(yr2020),
										}
									});
								}

								dataLinks.push({
									source: yr1995,
									target: yr2020,
									value: d.lcc_sqkm
								});
							});
							//console.log(dataNodes);

							function getLcColor(landCoverClass){
								switch(landCoverClass) {
									case "Natural / semi-natural land 1995":
									case "Natural / semi-natural land 2020":
										return "#538135";
									case "Mosaic natural / managed land 1995":
									case "Mosaic natural / managed land 2020":
										return "#cca533";
								  	case "Cultivated / managed land 1995":
									case "Cultivated / managed land 2020":
										return "#d07a41";
								  	case "Water / snow and ice 1995":
									case "Water / snow and ice 2020":
										return "#536fa0";
									default:
									  break;
							  }
							}
							
							//Chart
							var chartData = {
								color: "ignore",
								title: indicatorTitle,
								tooltip: {
									trigger: 'item',
									triggerOn: 'mousemove'
								},
								series: {
									type: 'sankey',
									// layout: 'none',
									emphasis: {
										focus: 'adjacency'
									},
									data: dataNodes,
									links: dataLinks
								},
								xAxis: {
									show: false
								},
								yAxis: {
									show: false
								}
							};					
							$().createXYAxisChart(cardTwigName , chartData);
						}
					);	
				});
				//! ## End Land Cover ESA Land Cover change 1995-2015  ##

				//! ## Start Species Species Richness  ##
				$('#collapseSpeciesLayers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesLayers'; //Set this once here as so there are no mistakes below.
				});
				//! ## End Species Species Richness  ##

				//! ## Start Species Species Reported number of animal and plant species ##
				$('#collapseSpeciesAnimalPlantNumbers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesAnimalPlantNumbers'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Reported number of animal and plant species in " + selSettings.countryName; 
					//var tableChartColors = ['rgb(127, 173, 65)'];
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$().insertBiopamaLoader(currentTableDiv);
						var speciesAnimalPlantNumbersUrl = DopaBaseUrl+'get_dopa_country_iucn_species?format=json&country_code='+selSettings.iso2;
						$.getJSON(speciesAnimalPlantNumbersUrl,function(d){
							var data = d.records[0];
							var dataSet = [['Number', data.iucn_animals_total, data.iucn_plants_total, data.iucn_species_total], ['Country Ranking', data.iucn_animals_total_rank, data.iucn_plants_total_rank, data.iucn_species_total_rank]];

							var columnData = [
								{title: ""},
								{title: "Animal species assessed by IUCN"},
								{title: "Plant species assessed by IUCN"},
								{title: "Total species assessed by IUCN"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ 1,2,3 ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData);
						});
					}
				});
				//! ## End Species Species Reported number of animal and plant species ##

				//! ## Start Species Reported number of threatened amphibians, birds and mammals ##
				$('#collapseSpeciesThreatNumbers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesThreatNumbers'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Reported number of threatened amphibians, birds and mammals " + selSettings.countryName; 
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$().insertBiopamaLoader(currentTableDiv);
						var speciesAnimalPlantNumbersUrl = DopaBaseUrl+'get_dopa_country_iucn_threatened_animals?format=json&country_code='+selSettings.iso2;
						$.getJSON(speciesAnimalPlantNumbersUrl,function(d){
							var data = d.records[0];
							var dataSet = [['Number', data.iucn_amphibians_threatened, data.iucn_birds_threatened, data.iucn_mammals_threatened, data.iucn_threatened_animals_total], ['Country Ranking', data.iucn_amphibians_threatened_rank, data.iucn_birds_threatened_rank, data.iucn_mammals_threatened_rank, data.iucn_threatened_animals_total_rank]];

							var columnData = [
								{title: ""},
								{title: "Threatened Amphibians"},
								{title: "Threatened Birds"},
								{title: "Threatened Mammals"},
								{title: "Total"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ 1,2,3,4 ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData);
						});
					}
				});
				//! ## End Species Reported number of threatened amphibians, birds and mammals ##

				//! ## Start Species Reported endemic and threatened endemic vertebrates in the country ##
				$('#collapseSpeciesThreatEndemicNumbers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesThreatEndemicNumbers'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Reported endemic and threatened endemic vertebrates in " + selSettings.countryName; 
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$().insertBiopamaLoader(currentTableDiv);
						var speciesAnimalPlantNumbersUrl = DopaBaseUrl+'get_dopa_country_iucn_threatened_endemic_vertebrates?format=json&country_code='+selSettings.iso2;
						$.getJSON(speciesAnimalPlantNumbersUrl,function(d){
							var data = d.records[0]; 
							var dataSet = [['Country Endemic', data.iucn_amphibians_endemic, data.iucn_birds_endemic, data.iucn_mammals_endemic, data.iucn_sharks_rays_chimaeras_endemic, data.iucn_endemic_vertebrates_total], ['Threatened Country Endemic', data.iucn_amphibians_endemic_threatened, data.iucn_birds_endemic_threatened, data.iucn_mammals_endemic_threatened, data.iucn_sharks_rays_chimaeras_endemic_threatened, data.iucn_endemic_threatened_vertebrates_total]];

							var columnData = [
								{title: ""},
								{title: "Amphibians"},
								{title: "Birds"},
								{title: "Mammals"},
								{title: "Sharks and Rays"},
								{title: "Total"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ 1,2,3,4,5 ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData);
						});
					}
				});
				//! ## End Species Reported endemic and threatened endemic vertebrates in the country ##

				//! ## Start Species Threatened and near threatened species (computed)  ##
				$('#collapseSpeciesComputed').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesComputed'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "Threatened and near threatened species (computed) for " + selSettings.countryName; 
					var speciesRedListUrl = DopaBaseUrl+'get_dopa_country_pa_redlist_list?format=json&country_code=' + selSettings.num;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(speciesRedListUrl,function(d){
							var dataSet = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								//newDataRow.push('<a href="https://www.iucnredlist.org/search?query='+data.id_no+'&searchType=species"target="_blank"><img class="iucn-redlist-img" src="'+imagePath+'redlist50sm.png" alt="RED LIST IUCN"></a>', data.binomial, data.kingdom, data.phylum,data.order, data.family, data.code);
								newDataRow.push('<a href="https://www.iucnredlist.org/search?query='+data.id_no+'&searchType=species"target="_blank">Link</a>', data.binomial, data.class, data.phylum, data.order, data.family, data.category);
								dataSet.push(newDataRow); 
							});
							var columnData = [
								{ title: "IUCN Red List" },
								{ title: "Scientific Name" },
								{ title: "Class" },
								{ title: "Phylum" },
								{ title: "Order" },
								{ title: "Family" },
								{ title: "Status" },
							];
							var columnSettings = [
								{
									"targets": 6,
									"className": "dt-center",
									"createdCell": function (td, cellData, rowData, row, col) { 
										switch(cellData) {
										  	case "EX":
												$(td).html('<div class="species-table-status species-ex">Extinct (EX)</div>');
												break;
										  	case "EW":
												$(td).html('<div class="species-table-status species-ew">Extinct in the Wild (EW)</div>');
												break;
											case "CR":
												$(td).html('<div class="species-table-status species-cr">Critically Endangered (CR)</div>');
												break;
											case "EN":
												$(td).html('<div class="species-table-status species-en">Endangered (EN)</div>');
												break;
											case "VU":
												$(td).html('<div class="species-table-status species-vu">Vulnerable (VU)</div>');
												break;
											case "NT":
												$(td).html('<div class="species-table-status species-nt">Near Threatened (NT)</div>');
												break;
											case "LC":
												$(td).html('<div class="species-table-status species-lc">Least Concern (LC)</div>');
												break;
										  	default:
												$(td).html('<div class="species-table-status species-dd">Data Deficient (DD)</div>');
										}
									},
								},
								{ "className": cardTwigName, "targets": [ "_all" ] }
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
						});
					}
				});
				//! ## End Species Threatened and near threatened species (computed)  ##

				//! ## Start Species occurrences reported to the GBIF ##
				$('#collapseSpeciesGbif').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesGbif'; //Set this once here as so there are no mistakes below.
					//
				});
				//! ## End Species occurrences reported to the GBIF ##

				//! ## Start Conservation Protected area coverage  ##
				$('#collapsePaProtCon').on('shown.bs.collapse', function () {
					var cardTwigName = 'PaProtCon'; //Set this once here as so there are no mistakes below.
					//
					var indicatorTitle = "Protected area coverage for " + selSettings.countryName; 
					var tableChartColors = [biopamaPAColors.terrestrial, biopamaPAColors.marine, biopamaPAColors.protConn];

					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var stats = countryProtStats.statistics;
						
						//Chart
						var chartData = {
							colors: tableChartColors,
							title: indicatorTitle,
							yAxis: {
								name: "Percent", 
							},
							legend: {}, 
							xAxis: {
								type: 'category',
								data: ['Protected Land Area', 'Protected Marine Area', 'Terrestrial Connectivity']
							},
							series: [{
							data: [{
								value: stats.percentage_pa_land_cover,
								name: stats.percentage_pa_land_cover + '%',
								itemStyle: {
									color: tableChartColors[0]
								}
							},{
								value: stats.percentage_pa_marine_cover,
								name: stats.percentage_pa_marine_cover + '%',
								itemStyle: {
									color: tableChartColors[1]
								}
							},{
								value: allCountryInds.protconn,
								name: stats.protconn + '%',
								itemStyle: {
									color: tableChartColors[2]
								}
							}],
							type: 'bar',
							}]
						}
						
						$().createXYAxisChart(cardTwigName , chartData); 

						var dataSet = [[stats.land_area, stats.pa_land_area, stats.percentage_pa_land_cover, stats.marine_area, stats.pa_marine_area, stats.percentage_pa_marine_cover, allCountryInds.protconn]];  

						var columnData = [
							{ title: "Total Land Area (km2)" },
							{ title: "Protected Land Area (km2)" },
							{ title: "Terrestrial Coverage (%)" },
							{ title: "Total Marine Area (km2)" },
							{ title: "Protected Marine Area (km2)" },
							{ title: "Marine Coverage (%)" },
							{ title: "Terrestrial Connectivity (%)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 6 ] },
							{ "className": "dt-center", "targets": "_all"}
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);

					}
				});
				//! ## End Conservation Protected area coverage  ##

				//! ## Start Conservation Country protection relative to Aichi Target 11  ##
				$('#collapseCountryAichiProt').on('shown.bs.collapse', function () {
					var cardTwigName = 'CountryAichiProt'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Protection relative to Aichi Target 11 for " + selSettings.countryName; 
					var tableChartColors = ['#1f814a','#1078b4'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[countryProtStats.statistics.percentage_pa_land_cover, countryProtStats.statistics.percentage_pa_marine_cover]]; 
						var columnData = [
							{ title: "Progress towards Aichi Target 11: 17% terrestrial coverage" },
            				{ title: "Progress towards Aichi Target 11: 10% marine coverage" },
						];
						var columnSettings = [{
							"targets": [ 0 ],
							"className": "dt-center "+cardTwigName+"-cell-color-0",
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 17){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 10 && cellData <  17) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 5 && cellData <  10) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  5) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  2) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(17-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						},{
							"targets": [ 1 ],
							"className": "dt-center "+cardTwigName+"-cell-color-1",
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 10){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 8 && cellData <  10) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 4 && cellData <  8) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  4) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  1) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(10-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						}];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				//! ## End Conservation Country protection relative to Aichi Target 11  ##

				//! ## Start Conservation Number of protected areas  ##
				$('#collapseNumPas').on('shown.bs.collapse', function () {
					var cardTwigName = 'NumPas'; //Set this once here as so there are no mistakes below.
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					var tableChartColors = ['#7fad41','#8fcde5','#e29864'];
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "Number of Terrestrial, Marine and Coastal protected areas for " + selSettings.countryName; 
					var paNumbersUrl = DopaBaseUrl+'get_dopa_country_pa_count?format=json&iso3=' + selSettings.iso3;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(paNumbersUrl,function(d){
							var dataSet = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(data.pa_count, data.pa_count_polygons, data.pa_count_points, data.pa_count_polygons_terrestrial, data.pa_count_points_terrestrial, data.pa_count_polygons_marine, data.pa_count_points_marine, data.pa_count_polygons_coastal, data.pa_count_points_coastal);
								dataSet.push(newDataRow); 
							});
							
							var columnData = [
								{ title: "Total number of all PAs (all geometries)" },
								{ title: "# all polygon PAs" },
								{ title: "# all point PAs" },
								{ title: "# <b>terrestrial</b> PAs (polygons)" },
								{ title: "# <b>terrestrial</b> PAs (points)" },
								{ title: "# <b>marine</b> PAs (polygons)" },
								{ title: "# <b>marine</b> PAs (points)" },
								{ title: "# <b>coastal</b> PAs (polygons)" },
								{ title: "# <b>coastal</b> PAs (points)" },
							];
							var columnSettings = [
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 4 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 6 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 7 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 8] },
								{ "className": "dt-center", "targets": "_all"}
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						});
					}
				});
				//! ## End Conservation Number of protected areas  ##

				//! ## Start Conservation List of protected areas  ##
				$('#collapseListPas').on('shown.bs.collapse', function () {
					var cardTwigName = 'ListPas'; //Set this once here as so there are no mistakes below.
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "List of protected areas for " + selSettings.countryName; 
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [];
						$(allCountryPaStats).each(function(i, data) {
							var newDataRow = [];
							newDataRow.push(data.wdpaid, data.pa_name, data.area_geo, data.nature);
							dataSet.push(newDataRow);
						});
						var columnData = [
							{ title: "WDPA ID" },
							{ title: "Name" },
							{ title: "Area (km\u00B2)" },
							{ title: "Type" },
						];
						var columnSettings = [
							{ "visible": false, "targets": [ 0 ] },
							{
								"targets": [ 1 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									$(cell).html('<a href="/ct/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
								} 
							},
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{
								"targets": [ 3 ],
								"className": "dt-center",
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									switch(cellData) {
										case "MA":
											$(cell).html('<div class="pa-type pa-marine">Marine</div>');
											break;
										case "TM":
											$(cell).html('<div class="pa-type pa-coastal">Coastal</div>');
											break;
										default:
											$(cell).html('<div class="pa-type pa-terrestrial">Terrestrial</div>');
									}
								} 
							}
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});
				//! ## End Conservation List of protected areas ##

				//! ## Start Conservation Key Biodiversity Areas  ##
				$('#collapseKBA').on('shown.bs.collapse', function () {
					var cardTwigName = 'KBA'; //Set this once here as so there are no mistakes below.
					//
					var indicatorTitle = "Number and protection of Key Biodiversity Areas for " + selSettings.countryName; 
					var tableChartColors = ['#65a70c','#e7aa27','#e36b0f','#1ba2c4'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.kba_n_tot, allCountryInds.kba_n_fully_prot, allCountryInds.kba_n_partially_prot, allCountryInds.kba_n_not_protected, allCountryInds.kba_avg_prot_perc_tot]]; 
						var columnData = [ 
							{ title: "KBAs in country <br>(#)" },
            				{ title: "KBAs fully protected <br>(# > 98%)" },
							{ title: "KBAs partially protected <br>(2% < # < 98%)" }, 
							{ title: "KBAs not protected <br>(# < 2%)" },
							{ title: "KBAs average protection <br>(%)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center", "targets": [ '_all' ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
					
				});
				//! ## End Conservation Key Biodiversity Areas  ##

				//! ## Start Conservation Estimated number of threatened and near threatened species for protected areas  ##
				$('#collapseEstPaSpeciesThreat').on('shown.bs.collapse', function () {
					var cardTwigName = 'EstPaSpeciesThreat'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "Estimated number of threatened and near threatened species\nin protected areas for " + selSettings.countryName; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					var paNumbersUrl = DopaBaseUrl+'get_dopa_country_pa_redlist_count?format=json&country_code=' + selSettings.iso2;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(paNumbersUrl,function(d){
							var dataSet = [];
							var chartXaxisData = [];
							var chartThreatSeriesData = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								//if (data.count_id == "-999") data.count_id = 0; 
								newDataRow.push(parseInt(data.wdpaid), data.pa_name, data.near_threatened, data.threatened, (data.near_threatened + data.threatened));
								dataSet.push(newDataRow);
								chartXaxisData.push(data.pa_name);
								chartThreatSeriesData.push((data.near_threatened + data.threatened));
							});
							chartThreatSeriesData.sort((a,b)=>a-b);
							chartThreatSeriesData.reverse();
							//Chart
							var chartData = {
								colors: tableChartColors,
								title: indicatorTitle,
								yAxis: {
									name: "Number (#)", 
								},
								xAxis: {
									type: 'category',
									data: chartXaxisData
								},
								series: [{
								  	data: chartThreatSeriesData,
								  	type: 'bar',
								}] 
							}
							$().createXYAxisChart(cardTwigName , chartData); 
							var columnData = [
								{ title: "WDPA ID" },
								{ title: "Name" },
								{ title: "Estimated # of near threatened species" },
								{ title: "Estimated # of threatened species" },
								{ title: "Total" },
							];
							//<a href="/wdpa/555563545" target="_blank">91 Ammunisie Depo Private Nature Reserve</a>
							var columnSettings = [
								{ "visible": false, "targets": [ 0 ] },
								{
									"targets": [ 1 ],
									"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
										$(cell).html('<a href="/ct/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
									} 
								},
								{ "className": "dt-center", "targets": [ 2, 3 ] },
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								defaultSort: [[ 3, "desc" ]],
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
						});
					}
				});
				//! ## End Conservation Estimated number of threatened and near threatened species for protected areas ##

				//! ## Start Conservation Habitat diversity in protected areas ≥ 5 km2  ##
				$('#collapsePaHabitatDiversity10').on('shown.bs.collapse', function () {
					var cardTwigName = 'PaHabitatDiversity10'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "Terrestrial and Marine Habitat Diversity Indicator for Protected Areas\nbigger than 5km in " + selSettings.countryName; 
					var tableChartColors = [biopamaPAColors.protConn];
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [];
						var chartXaxisData = [];
						var chartSeriesData = [];
						var sortedHdi = allCountryPaStats.sort( $().sortObject("hdi", "dec") );
						$(sortedHdi).each(function(i, data) {
							var newDataRow = [];
							if (data.hdi == "-999") data.hdi = 0; 
							newDataRow.push(data.wdpaid, data.pa_name, data.hdi);
							dataSet.push(newDataRow);
							chartXaxisData.push(data.pa_name);
							chartSeriesData.push(data.hdi);
						});
						//Chart
						var chartData = {
							colors: tableChartColors,
							title: indicatorTitle,
							yAxis: {
								name: "Number (#)", 
							},
							xAxis: {
								type: 'category',
								data: chartXaxisData,
								title: "Protected Area Name"
							},
							series: [{
							  data: chartSeriesData,
							  type: 'bar',
							}] 
						}
						$().createXYAxisChart(cardTwigName , chartData); 
						var columnData = [
							{ title: "WDPA ID" },
							{ title: "Protected Area Name" },
							{ title: "Habitat diversity" },
						];
						//<a href="/wdpa/555563545" target="_blank">91 Ammunisie Depo Private Nature Reserve</a>
						var columnSettings = [
							{ "visible": false, "targets": [ 0 ] },
							{
								"targets": [ 1 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									$(cell).html('<a href="/ct/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
								} 
							},
							{ "className": "dt-center", "targets": [ 2 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							defaultSort: [[ 2, "desc" ]],
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});
				//! ## End Conservation Habitat diversity in protected areas ≥ 5 km2 ##

				//! ## Start Pressures List of protected areas ≥ 10 km2 and associated pressures  ##
				$('#collapsePasPressures10').on('shown.bs.collapse', function () {
					var cardTwigName = 'PasPressures10'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "List of protected areas ≥ 1 km2 and associated pressures\nin " + selSettings.countryName; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [];
						$(allCountryPaStats).each(function(i, data) {
							var newDataRow = [];
							newDataRow.push(data.wdpaid, data.pa_name, data.area_geo, data.ap, data.roads_in, data.ppi, data.ppi_change, data.nature);
							dataSet.push(newDataRow);
						});
						var columnData = [
							{ title: "WDPA ID" },
							{ title: "Protected Area Name" },
							{ title: "Area (km\u00B2)" },
							{ title: "Agricultural Pressure" },
							{ title: "Road Pressure" },
							{ title: "Population Pressure" },
							{ title: "Population Density Change 2000-2020 (%)" },
							{ title: "Type" },
						];
						//<a href="/wdpa/555563545" target="_blank">91 Ammunisie Depo Private Nature Reserve</a>
						var columnSettings = [
							{ "visible": false, "targets": [ 0 ] },
							{
								"targets": [ 1 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									$(cell).html('<a href="/ct/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
								} 
							},
							{
								"targets": [ 7 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									switch(cellData) {
										case "MA":
											$(cell).html('<div class="pa-type pa-marine">Marine</div>');
											break;
										case "TM":
											$(cell).html('<div class="pa-type pa-coastal">Coastal</div>');
											break;
										default:
											$(cell).html('<div class="pa-type pa-terrestrial">Terrestrial</div>');
									}
								} 
							},
							{ "className": "dt-center", "targets": [ 2,3,4,5,6,7 ] },
						]; 
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							defaultSort: [[ 2, "desc" ]],
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});
				//! ## End Pressures List of protected areas ≥ 10 km2 and associated pressures ##

				//! ## Start Pressures Working poor at $3.10 a day (% of total employment)  ##
				$('#collapseWorkingPoor').on('shown.bs.collapse', function () {
					var cardTwigName = 'WorkingPoor'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Working poor at $3.10 a day (% of total employment)\nin " + selSettings.countryName; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					//
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$().insertBiopamaLoader(currentTableDiv);
						//https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_ecoregions_stats?format=json&c_un_m49=516
						var workingPoorUrl = 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dopa_explorer_3:undp_poverty_employed_people&maxFeatures=50&outputFormat=application%2Fjson&propertyName=a_1991,a_1992,a_1993,a_1994,a_1995,a_2000,a_2005,a_2010,a_2011,a_2012,a_2013,mean,HDI_Rank&CQL_FILTER=un_m49='+selSettings.num;
						$.getJSON(workingPoorUrl,function(d){
							var data = d.features[0].properties
							var dataSet = [["1991", data.a_1991], ["1992", data.a_1992], ["1993", data.a_1993], ["1994", data.a_1994], ["1995", data.a_1995], ["2000", data.a_2000], ["2005", data.a_2005], ["2010", data.a_2010], ["2011", data.a_2011], ["2012", data.a_2012], ["2013", data.a_2013]];
							var chartXaxisData = ["1991", "1992", "1993", "1994", "1995", "2000", "2005", "2010", "2011", "2012", "2013"];
							var chartSeriesData = [data.a_1991, data.a_1992, data.a_1993, data.a_1994, data.a_1995, data.a_2000, data.a_2005, data.a_2010, data.a_2011, data.a_2012, data.a_2013];
							//Chart
							var chartData = {
								colors: tableChartColors,
								title: indicatorTitle,
								yAxis: {
									name: "Percent (%)", 
								},
								xAxis: {
									type: 'category',
									data: chartXaxisData,
									title: "Year" 
								},
								series: [{
								  data: chartSeriesData,
								  type: 'bar',
								  markLine: {
									  data: [{
										type: "average",
										name: 'Average',
									  }],
									  label: {
										show: true,
										position: "insideEndTop",
										formatter: '{b}: {c}%',
									  }
								  },
								}] 
							}
							$().createXYAxisChart(cardTwigName , chartData); 
							var columnData = [
								{title: "Year"},
								{title: "Working Poor (%)"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ '_all' ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData);
						});
					}
				});
				//! ## End Pressures Working poor at $3.10 a day (% of total employment) ##
		    });
		}
    };
})(jQuery, Drupal);
