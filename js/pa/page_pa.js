/* 

JS file for Protected Areas 

*/
 
 /* 
 all cards and data are found in drupalSettings.dataCards
 
 console.log(drupalSettings.dataCards ); //to find your data
 */
var thisWKT; //eStation module

function updatePaFromPopup(){
	updatePaPageURL(selSettings.WDPAID);
	//if the PA tab is not active when the PA changes we must select it to give the page time to figure out the country codes
	if(jQuery(".indi-tab-local[aria-expanded='false']").length){
		jQuery("#indicator-card-tabs > #tabs-4").collapse("toggle");
	} 
}
function updateCountryFromPopup(){
	getRestResults();
}
function updatePaPageURL(wdpaid){
	const state = { wdpaid: wdpaid };
	history.pushState(state, '', '/ct/pa/' + wdpaid); 
	window.dispatchEvent(new Event('popstate'));
}

(function ($, Drupal) {
 
	var pathArray = window.location.pathname.split('/');
	selSettings.WDPAID = pathArray[pathArray.length - 1]; //gets the last thing in the URL (should be a WDPA ID)

	var allInds = []; //Country Indicators
	var paChanged = 1;

	var popupOptions = {
		countryLink: '/ct/country/',
		paLink: '',
	}
	$().addMapLayerInteraction(mymap, popupOptions);

	getIndicatorData(selSettings.WDPAID); //We run the big indicator call right away and try to get all our data in the background. 
	function getIndicatorData(wdpaid){
		var allIndicatorsURL = DopaBaseUrl + 'get_dopa_wdpa_all_inds?format=json&wdpaid=' + wdpaid;
		//for the eStation modlue
		var eStationIsEnabled = drupalSettings.eStation;
		var globalDataPath = drupalSettings.globalDataPath;
		var WKTurl = globalDataPath + "/js/pa/empty.json";
		if (eStationIsEnabled){
			//WKTurl = "https://geospatial.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=marxan:wdpa_latest_biopama&propertyname=wdpaid,name,iso3,wkt_geometry&SORTBY=wdpaid&CQL_FILTER=wdpaid="+wdpaid+"&outputFormat=application%2Fjson"	
			WKTurl = "https://geospatial2.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=biopama:wdpa_latest_biopama_wkt&propertyname=wdpaid,name,iso3,wkt_geometry&SORTBY=wdpaid&CQL_FILTER=wdpaid="+wdpaid+"&outputFormat=application%2Fjson"	
		}
		//eStation end
		$('.indicatorProccessed').removeClass('indicatorProccessed');
		$.when(
			$.getJSON(allIndicatorsURL,function(d){
				allInds = d['records'][0];
				selSettings.WDPAID = allInds.wdpaid; 
				selSettings.ISO3 = allInds.iso3; 
				if (mymap.getLayer("wdpaAcpSelected")) {
					mymap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');
					mymap.setFilter('wdpaAcpSelected', ['==', 'wdpaid', selSettings.WDPAID]);	
				}
				paChanged = 1;
			}),
			//for the eStation modlue
			$.getJSON(WKTurl,function(d){
				if (!$.isEmptyObject(d)){
					thisWKT = d['features'][0]['properties']['wkt_geometry']
				}
			}).fail(function (result) {
				console.log('unable to get WKT'); // this is hit for return Json(null); and return null;
			}),
			//eStation end
		).then(function() {
			$().mapZoomToPA(mymap, selSettings.WDPAID);
			$( ".wrapper-data-card.show" ).each(function() {  
				$( this ).trigger("shown.bs.collapse"); 
			});
			populateTopInfo();
			if ($(".ctt-card-title[aria-expanded='true']").length) {
				getRestResults();
			}
			//for the eStation modlue
			if ($(".estation-card-title[aria-expanded='true']").length > 0){
				$(".estation-years").trigger('change');
			}
			//eStation end
		});
	}
	mymap.on('moveend', function () {
		//this is really just for when the page first loads
		if (paChanged == 1){	
			updatePA();
		}
		var relatedFeatures = mymap.querySourceFeatures("BIOPAMA_Poly",{
			sourceLayer: biopamaGlobal.map.layers.country,
			filter: ['==', 'iso3', selSettings.ISO3]
		});
		//Here we update all of our global settings by getting them from the EEZ/Country layer
		//console.log(relatedFeatures[0].properties)
		selSettings.countryName = relatedFeatures[0].properties.country_name; 
		selSettings.ISO2 = relatedFeatures[0].properties.iso2;
		selSettings.ISO3 = relatedFeatures[0].properties.iso3;
		selSettings.NUM = relatedFeatures[0].properties.un_m49;
		selSettings.regionName = relatedFeatures[0].properties.Group;
	});

	function populateTopInfo(){ 
		/**
		 * 
		 * The data at the top of the country page section goes here.
		 * 
		 */
		//
		var PAtopData = "https://geospatial.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=marxan:wdpa_latest_biopama&propertyname=wdpaid,iucn_cat,name,desig_eng,desig_type,marine,rep_area,gis_area,status_yr,mang_auth&CQL_FILTER=wdpaid=" + selSettings.WDPAID + "&outputFormat=application%2Fjson";
		$.getJSON(PAtopData,function(d){
			$(".biopama-collapse[data-bs-target='#collapseMarineandCoastal']").attr('style','display:flex !important');
			$("#card-title-Oceanography").closest(".card").show(); //eStation
			var data = d['features'][0]['properties']; 
			$(".top-pa-wdpa").text(data.wdpaid);
			$(".top-pa-name").text(data.name);
			$(".top-pa-desig-eng").text(data.desig_eng);
			$(".top-pa-desig-type").text(data.desig_type);
			$(".top-pa-rep-area").text((data.rep_area).toFixed(2));
			$(".top-pa-gis-area").text((data.gis_area).toFixed(2));
			$(".top-pa-status-yr").text(data.status_yr);
			$(".top-pa-mang-auth").text(data.mang_auth);
			$(".top-pa-iucn-cat").text(data.iucn_cat);
			$("a.top-ap-pp-url").attr("href", 'https://www.protectedplanet.net/' + data.wdpaid);
			switch(data.marine){
				case "0":
					$(".top-pa-marine").html('<span style="color: ' + biopamaPAColors.terrestrial + ';"> Terrestrial </span>');
					$(".biopama-collapse[data-bs-target='#collapseMarineandCoastal']").attr('style','display:none !important');
					$("#card-title-Oceanography").closest(".card").hide(); //eStation
					break;
				case "1":
					$(".top-pa-marine").html('<span style="color: ' + biopamaPAColors.marine + ';"> Marine </span>');
					break;
				default:
					$(".top-pa-marine").html('<span style="color: ' + biopamaPAColors.costal + ';"> Costal </span>');
			}
			if (data.mang_auth == "Not Reported"){
				$(".top-pa-mang-auth").html("the management authority is not reported");
			} else {
				$(".top-pa-mang-auth").html("it is managed by <b>" + data.mang_auth + "</b>");
			}
		});
	};

	function updatePA(){ 
		if (mymap.getLayer("wdpaAcpSelected")) {
			mymap.setFilter('wdpaAcpSelected', ['==', 'wdpaid', selSettings.WDPAID]);	
			mymap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');
		}
		//$('.scope-name').text(selSettings.paName );
		document.title = 'Protected Area Data | ' + selSettings.paName ;
		paChanged = 0;
	};

	window.addEventListener('popstate', function (event) {
		var pathArray = window.location.pathname.split('/');
		var pathWDPAID = pathArray[pathArray.length - 1]; //gets the last thing in the URL (should be a country code)
		selSettings.WDPAID = pathWDPAID;
		getIndicatorData(selSettings.WDPAID);
	}, false);
	
    Drupal.behaviors.biopamaDopa = {
		attach: function (context, settings) {
		    $(window).once().on('load scroll', function () {
				
				$( ".report-icon" ).click(function( event ) {
				  	$(this).toggleClass("report-added");
					var reportCardID = $(this).closest('.card-text').prev().attr("data-bs-target");
					var reportCardPart = 'Chart';
					if ( $(this).next().hasClass('wrapper-data-table') ) {
						reportCardPart = 'Table';
					}
					var indicator = reportCardID.replace("#collapse", "");
					if ($(this).hasClass("report-added")){
						$().addIndicatorToReport(indicator, reportCardPart);
					} else {
						$().removeIndicatorFromReport(indicator, reportCardPart); 
					}
				});
				
				// #### Card Start  - we go through every card and attach the functions needed to generate the content

/* ################################################ Climate Elevation Profile  ###################################################### */
				// $('#collapseElevationProfile').on('shown.bs.collapse', function () {
				// 	var cardTwigName = 'ElevationProfile'; //Set this once here as so there are no mistakes below.
					 
				// 	var indicatorTitle = "Virtual elevation profile for " + selSettings.paName; 
				// 	var tableChartColors = ['#65a70c'];
				// 	//Chart
				// 	var chartData = {
				// 		colors: tableChartColors,
				// 		title: indicatorTitle,
				// 		yAxis: {
				// 			name: "Elevation (m) above sea level", 
				// 		},
				// 		xAxis: {
				// 			type: 'category',
				// 			data: [ 'Min.' , 'Max.' ]
				// 		},
				// 		series: [{
				// 		  name: 'Elevation',
				// 		  data: [allInds.elev_min, allInds.elev_max],
				// 		  type: 'line',
				// 		  areaStyle: {
				// 			color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
				// 				offset: 0,
				// 				color: '#65a70c'
				// 			}, {
				// 				offset: 1,
				// 				color: '#ffe'
				// 			}])
				// 		  },
				// 		  label: {
				// 			  show: true,
				// 			  formatter: function(d) { 
				// 				return d.name + ' ' + d.data + 'm';
				// 			  }
				// 		  },
				// 		  markLine: {
				// 			  data: [{
				// 				type: "average",
				// 				name: 'Average',
				// 			  }],
				// 			  label: {
				// 				show: true,
				// 				position: "insideEndTop",
				// 				formatter: '{b}: {c}m',
				// 			  },
				// 			  lineStyle: {
				// 				  color: '#65a70c',
				// 			  }
				// 		  },
				// 		}]
				// 	}
				// 	$().createXYAxisChart(cardTwigName , chartData);  
					
				// 	//Datatable Simple
				// 	if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
				// 		var dataSet = [[allInds.elev_min, allInds.elev_mean, allInds.elev_max]]; 
				// 		var columnData = [
				// 			{ title: "Min. (m)" },
            	// 			{ title: "Mean (m)" },
            	// 			{ title: "Max. (m)" },
				// 		];
				// 		var columnSettings = [{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": "_all" }];
				// 		var tableData = {
				// 			title: indicatorTitle,
				// 			columns: columnData,
				// 			columnDefs: columnSettings,
				// 			data: dataSet,
				// 			attribution: dopaAttribution,
				// 			isComplex: false
				// 		}
				// 		$().createDataTable('#data-table-'+cardTwigName, tableData); 
				// 		// $().updateCellColors(cardTwigName, tableChartColors);
				// 	}
				// });


/* ######################################################## Average Climate ######################################################### */
				$('#collapseAverageClimate').on('shown.bs.collapse', function () {
					var cardTwigName = 'AverageClimate'; //Set this once here as so there are no mistakes below.
					 
					var indicatorTitle = "Monthly climate average for " + selSettings.paName; 
					var tableChartColors = ['#65a70c', 'red', 'yellow', 'blue'];
					//Chart
					var chartData = {
						color: tableChartColors,
						title: indicatorTitle,
						legend: {
							data: ['Precipitation', 'Max Temperature', 'Mean Temperature', 'Min Temperature'],
							y: 'bottom'
						},
						yAxis: [
							{
								name: "Precipitation (mm)",
								position: "right",
								alignTicks: true,
								nameLocation: 'middle',
								nameGap: 70,
								axisLabel: {
									formatter: '{value} mm',
									color: biopamaGlobal.chart.colors.text
								}
							},
							{
								name: "Temperature (C°)", 
								position: "left",
								alignTicks: true,
								nameLocation: 'middle',
								nameGap: 50,
								axisLabel: {
									formatter: '{value} °C',
									color: biopamaGlobal.chart.colors.text
								}
							}
						],
						xAxis: [
							{
								type: 'category',
								data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
							}
						],
						series: [
							{
								name: 'Precipitation (mm)',
							  	type: 'bar',
								data: [allInds.cli_prec_01, allInds.cli_prec_02, allInds.cli_prec_03, allInds.cli_prec_04, allInds.cli_prec_05, allInds.cli_prec_06, allInds.cli_prec_07, allInds.cli_prec_08, allInds.cli_prec_09, allInds.cli_prec_10, allInds.cli_prec_11, allInds.cli_prec_12]
						  	},
							{
								name: "Max Temperature", 
								type: 'line',
								yAxisIndex: 1,
								data: [allInds.cli_tmax_01, allInds.cli_tmax_02, allInds.cli_tmax_03, allInds.cli_tmax_04, allInds.cli_tmax_05, allInds.cli_tmax_06, allInds.cli_tmax_07, allInds.cli_tmax_08, allInds.cli_tmax_09, allInds.cli_tmax_10, allInds.cli_tmax_11, allInds.cli_tmax_12]
							},
							{
								name: "Mean Temperature", 
								type: 'line',
								yAxisIndex: 1,
								data: [allInds.cli_tmean_01, allInds.cli_tmean_02, allInds.cli_tmean_03, allInds.cli_tmean_04, allInds.cli_tmean_05, allInds.cli_tmean_06, allInds.cli_tmean_07, allInds.cli_tmean_08, allInds.cli_tmean_09, allInds.cli_tmean_10, allInds.cli_tmean_11, allInds.cli_tmean_12]
							},
							{
								name: "Min Temperature", 
								type: 'line',
								yAxisIndex: 1,
								data: [allInds.cli_tmin_01, allInds.cli_tmin_02, allInds.cli_tmin_03, allInds.cli_tmin_04, allInds.cli_tmin_05, allInds.cli_tmin_06, allInds.cli_tmin_07, allInds.cli_tmin_08, allInds.cli_tmin_09, allInds.cli_tmin_10, allInds.cli_tmin_11, allInds.cli_tmin_12]
							}
						]
					}
					$().createXYAxisChart(cardTwigName , chartData);  
				});


// ############################################ Ecosystem Services Below Ground Carbon #####################################################
				$('#collapseBelowGroundCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'BelowGroundCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Below ground carbon in " + selSettings.paName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.bgb_min_c_mg, allInds.bgb_mean_c_mg, allInds.bgb_max_c_mg, allInds.bgb_tot_c_mg]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Mg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						// $().updateCellColors(cardTwigName, tableChartColors);
					}
				});

// ############################################ Ecosystem Services Soil Organic Carbon #####################################################
				$('#collapseSoilOrganicCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'SoilOrganicCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Soil organic carbon in " + selSettings.paName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.gsoc_min_c_mg, allInds.gsoc_mean_c_mg, allInds.gsoc_max_c_mg, allInds.gsoc_tot_c_mg]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Mg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0, 1, 2, 3 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						// $().updateCellColors(cardTwigName, tableChartColors);
					}
				});

// ############################################ Ecosystem Services Above ground carbon #####################################################
				$('#collapseAboveGroundCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'AboveGroundCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Above ground carbon in " + selSettings.paName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.agb_min_c_mg, allInds.agb_mean_c_mg, allInds.agb_max_c_mg, allInds.agb_tot_c_mg]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Mg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						// $().updateCellColors(cardTwigName, tableChartColors);
					}
				});

// ############################################ Ecosystem Services Total carbon #####################################################
				$('#collapseTotalCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'TotalCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Total carbon in " + selSettings.paName; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.carbon_min_c_mg, allInds.carbon_mean_c_mg, allInds.carbon_max_c_mg, allInds.carbon_tot_c_mg]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
							{ title: "Sum (Mg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						// $().updateCellColors(cardTwigName, tableChartColors);
					}
				});
			 
/* ######################################### Ecoregions overlapping with the protected area  ###################################################### */
				$('#collapseEcoregionsOverlappingProtectedAreas').on('shown.bs.collapse', function () {
					var indicatorTitle = "Ecoregions overlapping with " + selSettings.paName;
					var cardTwigName = 'EcoregionsOverlappingProtectedAreas'; //Set this once here as so there are no mistakes below.					

					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);

					$.getJSON(
						DopaBaseUrl + "get_dopa_ecoregion_pa?format=json&wdpaid=" + selSettings.WDPAID,
						function(d){
							var dataSet = [];

							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(
									data.eco_name,
									(data.marine === 'true')? 'Marine' : 'Terrestrial'
								);
								dataSet.push(newDataRow); 
							});

							var columnData = [
								{title: "Name"},
								{title: "Type"}
							];
							var columnSettings = [
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
							];
							var tableData = {
								title: indicatorTitle,
								columnDefs: columnSettings,
								columns: columnData,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
							// $().updateCellColors(cardTwigName, tableChartColors);
						});
				});

/* ##################################################### Forest cover  ############################################################################# */
				$('#collapseForestCover').on('shown.bs.collapse', function () {
					var thisUrl = biopamaApiUrl+'/api/forest/function/api_wdpa_tree_cover_loss_2001_2020/wdpaid='+selSettings.WDPAID;
					$.getJSON(thisUrl,function(d){
						data = d[0];
						var cardTwigName = 'ForestCover'; //Set this once here as so there are no mistakes below.
					
						var indicatorTitle = "Forest loss in " + selSettings.paName + " since 2000"; 
						var tableChartColors = ['#ff0000'];
						//Chart
						var chartData = {
							colors: tableChartColors,
							title: indicatorTitle,
							yAxis: {
								name: "Percent", 
							},
							xAxis: {
								type: 'category',
								data: ['Tree cover loss']
							},
							series: [{
							  data: [{
								  value: data.percentage_tree_loss_since_2000	,
								itemStyle: {
									color: tableChartColors[0]
								  }
							  }],
							  type: 'bar',
							}] 
						}
						
						$().createXYAxisChart(cardTwigName , chartData); 
						
						if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
							var dataSet = [[data.cover_area_sqkm_2000, data.tree_cover_loss_2001_2021, data.percentage_tree_loss_since_2000]]; 
							var columnData = [
								{ title: "Forest cover 2000 (km\u00B2)" },
								{ title: "Loss in protected areas 2000-2021 (km\u00B2)" },
								{ title: "Percentage loss in protected areas (%)" },
							];
							var columnSettings = [
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
								{ "className": "dt-center", "targets": "_all"}
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						}
					});
				});

/* ##################################################### Inland Surface Water  ############################################################################# */
				$('#collapseInlandSurfaceWater').on('shown.bs.collapse', function () {
					var cardTwigName = 'InlandSurfaceWater'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Area of Permanent and Seasonal Water in " + selSettings.paName; 
					var tableChartColors = ['#1710f7','#c184c1']; //this will be used to coordinate the coloring of BOTH table and chart
					//Chart
					var chartData = {
						color: tableChartColors,
						title: indicatorTitle,
						yAxis: {
							name: "Area (km\u00B2)"
						},
						xAxis: {
							type: 'category',
							data: ['1984','2018']
						},
						series: [{
						  name: 'Permanent Water',
						  data: [ parseFloat(allInds.water_p_now_sqkm)-(parseFloat(allInds.water_p_netchange_sqkm)) , allInds.water_p_now_sqkm],
						  type: 'bar',
						},{
						  name: 'Seasonal  Water',
						  data: [ parseFloat(allInds.water_s_now_sqkm)-(parseFloat(allInds.water_s_netchange_sqkm)) , allInds.water_s_now_sqkm],
						  type: 'bar',
						}]
					}					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.water_p_now_sqkm, allInds.water_s_now_sqkm, allInds.water_p_netchange_sqkm, allInds.water_s_netchange_sqkm, allInds.water_p_netchange_perc_first_epoch, allInds.water_s_netchange_perc_first_epoch]]; 
						var columnData = [
							{ title: "Area (km\u00B2) of permanent surface water (2018)" },
            				{ title: "Area (km\u00B2) of seasonal inland water (2018)" },
            				{ title: "Net change (km\u00B2) of permanent surface water (2018 – 1984)" },
            				{ title: "Net change (km\u00B2) of seasonal inland water " },
							{ title: "Net change (%) of permanent surface water (2018 – 1984)" },
							{ title: "Net change (%) in surface area of seasonal inland water " },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 0, 2, 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1, 3, 5 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						// $().updateCellColors(cardTwigName, tableChartColors);
					}
				});

/* ################################################### Habitat Diversity ########################################################################### */
				$('#collapseHabitatDiversity').on('shown.bs.collapse', function () {
					var cardTwigName = 'HabitatDiversity'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Area of Permanent and Seasonal Water in " + selSettings.paName; 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.hdi_freq, allInds.hdi_awhd]]; 
						var columnData = [
							{ title: "Number of Segments" },
            				{ title: "Terrestrial Habitat Diversity Indicator" }
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] }
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});

/* ################################################### Land Degradation ########################################################################### */
				$('#collapseLandDegradation').on('shown.bs.collapse', function () {
					var cardTwigName = 'LandDegradation'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Land degradation for " + selSettings.paName; 
					var tableChartColors = ['#ab2828', '#ed7325', '#ffd954', '#b8d879', '#46a246','#c2c5cc'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle, 
						series: [{
							// name: indicatorTitle,
							type: 'pie',
							radius: ['50%', '70%'],
							avoidLabelOverlap: true,
							data: [
								{value: allInds.lpd_severe_sqkm, name: 'Persistent severe decline in productivity'},
								{value: allInds.lpd_moderate_sqkm, name: 'Persistent moderate decline in productivity'},
								{value: allInds.lpd_stable_stressed_sqkm, name: 'Stable, but stressed'},
								{value: allInds.lpd_stable_sqkm, name: 'Stable Productivity'},
								{value: allInds.lpd_increased_sqkm, name: 'Persistent increase in productivity'},
								{value: allInds.lpd_null_sqkm, name: 'No biomass'},
							] 
						}]
					}
					
					$().createNoAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.lpd_null_sqkm, allInds.lpd_severe_sqkm, allInds.lpd_moderate_sqkm, allInds.lpd_stable_stressed_sqkm, allInds.lpd_stable_sqkm, allInds.lpd_increased_sqkm]]; 
						var columnData = [
							{ title: "No biomass (km\u00B2)" },
            				{ title: "Persistent severe decline in productivity (km\u00B2)" },
            				{ title: "Persistent moderate decline in productivity (km\u00B2)" },
            				{ title: "Stable, but stressed; persistent strong inter-annual productivity variations (km\u00B2)" },
							{ title: "Stable Productivity (km\u00B2)" },
							{ title: "Persistent increase in productivity (km\u00B2)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-5", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-4", "targets": [ 5 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						// $().updateCellColors(cardTwigName, tableChartColors);
					}
				});

/* ################################################### Land Fragmentation ######################################################################### */
				$('#collapseLandFragmentation').on('shown.bs.collapse', function () {
					var cardTwigName = 'LandFragmentation'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Natural land pattern fragmentation in " + selSettings.paName + " (1995-2015)";
					var tableChartColors = ['#7fad41', '#878787', '#52781f', '#f1c200', '#e89717','#d56317'];
					
					//an object is used here to calculate the percentages.
					var landFragmentationData = {
						core: [allInds.mspa_core_1995_sqkm, allInds.mspa_core_2000_sqkm, allInds.mspa_core_2005_sqkm, allInds.mspa_core_2010_sqkm, allInds.mspa_core_2015_sqkm],
						non_natural: [allInds.mspa_non_natural_1995_sqkm, allInds.mspa_non_natural_2000_sqkm, allInds.mspa_non_natural_2005_sqkm, allInds.mspa_non_natural_2010_sqkm, allInds.mspa_non_natural_2015_sqkm],
						edges: [allInds.mspa_edge_1995_sqkm, allInds.mspa_edge_2000_sqkm, allInds.mspa_edge_2005_sqkm, allInds.mspa_edge_2010_sqkm, allInds.mspa_edge_2015_sqkm],
						perforation: [allInds.mspa_core_perforation_1995_sqkm, allInds.mspa_core_perforation_2000_sqkm, allInds.mspa_core_perforation_2005_sqkm, allInds.mspa_core_perforation_2010_sqkm, allInds.mspa_core_perforation_2015_sqkm],
						islets: [allInds.mspa_islet_1995_sqkm, allInds.mspa_islet_2000_sqkm, allInds.mspa_islet_2005_sqkm, allInds.mspa_islet_2010_sqkm, allInds.mspa_islet_2015_sqkm],
						margins: [allInds.mspa_linear_1995_sqkm, allInds.mspa_linear_2000_sqkm, allInds.mspa_linear_2005_sqkm, allInds.mspa_linear_2010_sqkm, allInds.mspa_linear_2015_sqkm],
					};

					//Chart
					var chartData = {
						color: tableChartColors,
						title: indicatorTitle,
						legend: {
							data: ['Core', 'Non Natural', 'Edge', 'Core Perforation', 'Islet', 'Linear'],
							y: 'top'
						},
						yAxis: {
							name: "km\u00B2"
						},
						xAxis: {
							type: 'category',
							data: ['1995','2000','2005','2010','2015']
						},
						series: [{
							name: 'Core',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.core,
						}, {
							name: 'Non Natural',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.non_natural,
						}, {
							name: 'Edge',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.edges,
						}, {
							name: 'Core Perforation',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.perforation,
						},{
							name: 'Islet',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.islets,
						}, {
							name: 'Linear',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.margins,
						}]
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [
							[1995, allInds.mspa_core_1995_sqkm, allInds.mspa_non_natural_1995_sqkm, allInds.mspa_edge_1995_sqkm, allInds.mspa_core_perforation_1995_sqkm, allInds.mspa_islet_1995_sqkm, allInds.mspa_linear_1995_sqkm],
							[2000, allInds.mspa_core_2000_sqkm, allInds.mspa_non_natural_2000_sqkm, allInds.mspa_edge_2000_sqkm, allInds.mspa_core_perforation_2000_sqkm, allInds.mspa_islet_2000_sqkm, allInds.mspa_linear_2000_sqkm],
							[2005, allInds.mspa_core_2005_sqkm, allInds.mspa_non_natural_2005_sqkm, allInds.mspa_edge_2005_sqkm, allInds.mspa_core_perforation_2005_sqkm, allInds.mspa_islet_2005_sqkm, allInds.mspa_linear_2005_sqkm],
							[2010, allInds.mspa_core_2010_sqkm, allInds.mspa_non_natural_2010_sqkm, allInds.mspa_edge_2010_sqkm, allInds.mspa_core_perforation_2010_sqkm, allInds.mspa_islet_2010_sqkm, allInds.mspa_linear_2010_sqkm],
							[2015, allInds.mspa_core_2015_sqkm, allInds.mspa_non_natural_2015_sqkm, allInds.mspa_edge_2015_sqkm, allInds.mspa_core_perforation_2015_sqkm, allInds.mspa_islet_2015_sqkm, allInds.mspa_linear_2015_sqkm]];  
						var columnData = [
							{ title: "Year" },
							{ title: "Core (km\u00B2)" },
            				{ title: "Non Natural (km\u00B2)" },
            				{ title: "Edge (km\u00B2)" },
            				{ title: "Core Perforation (km\u00B2)" },
							{ title: "Islet (km\u00B2)" },
							{ title: "Linear (km\u00B2)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-4", "targets": [ 5 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-5", "targets": [ 6 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});

// ################################################# Agricultural Pressure ##############################################################
				$('#collapseAgriculturalPressure').on('shown.bs.collapse', function () {
					var cardTwigName = 'AgriculturalPressure'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Agricultural Pressure for " + selSettings.paName; 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.p_agriculture_pa_perc_tot, allInds.p_agriculture_bu_perc_tot]]; 
						var columnData = [
							{title: "Protected Area (%)"},
            				{title: "10 km Unprotected Buffer (%)"}
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});

// ################################################# Road Pressure ##############################################################
				$('#collapseRoadPressure').on('shown.bs.collapse', function () {
					var cardTwigName = 'RoadPressure'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Road Pressure for " + selSettings.paName; 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.p_road_pa_perc_tot, allInds.p_road_bu_perc_tot]]; 
						var columnData = [
							{title: "Protected Area (%)"},
							{title: "10 km Unprotected Buffer (%)"}
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});

// ################################################# Population Pressure ##############################################################
				$('#collapsePopulationPressure').on('shown.bs.collapse', function(){

					var cardTwigName = 'PopulationPressure'; //Set this once here as so there are no mistakes below.					
					var indicatorTitle = "Population Pressure for " + selSettings.paName; 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[
							allInds.p_population_pa_last_epoch_sum, allInds.p_population_pa_last_epoch_density, allInds.p_population_pa_density_change, allInds.p_population_pa_change_perc_first_epoch, 
							allInds.p_population_bu_last_epoch_sum, allInds.p_population_bu_last_epoch_density, allInds.p_population_bu_density_change, allInds.p_population_bu_change_perc_first_epoch
						]];
						var columnData = [
							{title: "Protected Area (people)"},
							{title: "Protected Area (density)"},
							{title: "Protected Area (people/km2)"},
							{title: "Protected Area (%)"},
							{title: "10 km Unprotected Buffer (people)"},
							{title: "10 km Unprotected Buffer (density)"},
							{title: "10 km Unprotected Buffer (people/km2)"},
							{title: "10 km Unprotected Buffer (%)"}
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}

				});

// ################################################# BuiltupAreas Pressure ##############################################################
				$('#collapseBuiltupAreasPressure').on('shown.bs.collapse', function () {
					var cardTwigName = 'BuiltupAreasPressure'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Built-up Areas Pressure for " + selSettings.paName; 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allInds.p_builtup_pa_sqkm, allInds.p_builtup_pa_perc_land, allInds.p_builtup_bu_sqkm, allInds.p_builtup_bu_perc_land]]; 
						var columnData = [
							{title: "Protected Area (km2)"},
							{title: "Protected Area (%)"},
							{title: "10 km Unprotected Buffer (km2)"},
							{title: "10 km Unprotected Buffer (%)"}
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": "_all" },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable('#data-table-'+cardTwigName, tableData); 
					}
				});

// ################################################# Anthropogenic Pressure ##############################################################
				$('#collapseAnthropogenicPressure').on('shown.bs.collapse', function(){
					$.getJSON(
						DopaBaseUrl41 + "get_de_wdpa_all_normalized_pressures?format=json&wdpaid="+selSettings.WDPAID,
						function(response){
							let data = {
								country_avg: [],
								site_norm_value: [],
								buffer_value: []
							};

							//[country_avg, site_norm_value, buffer_value]
							response.records.forEach(
								function(d){
									data.country_avg.push(d.country_avg);
									data.site_norm_value.push(d.site_norm_value);
									data.buffer_value.push(d.buffer_value);
								}
							);
							
							var cardTwigName = 'AnthropogenicPressure'; //Set this once here as so there are no mistakes below.
							var indicatorTitle = 'Anthropogenic Pressure'
							//Datatable Simple
							if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
								var chartData = {
									title: indicatorTitle,
									color: ['#dcdcdc', '#7fad41', '#bc661e'],
									yAxis: {
										name: "Values"
									},
									xAxis: {
										type: 'category',
										data: ['Agriculture', 'Built-up', 'Population', 'Pop. Change', 'Road']
									},
									legend: {
										data: ['Country Average', 'Protected Area', 'Unprotected Buffer'],
										y: 'top'
									},
									series: [
										{
											name: 'Country Average',
											type: 'bar',
											data: data.country_avg
										},
										{
											name: 'Protected Area',
											type: 'bar',
											data: data.site_norm_value
										},
										{
											name: 'Unprotected Buffer',
											type: 'bar',
											data: data.buffer_value
										}
									]
								}
								
								$().createXYAxisChart(cardTwigName , chartData); 
							}
						}
					);
				});

// ##################################################### Copernicus Global Land Cover 2019 ##########################################################
				$('#collapseCopernicusGLC').on('shown.bs.collapse', function () {
					var cardTwigName = 'CopernicusGLC'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Copernicus Land Cover for " + selSettings.paName; 
					let copernicusLandCoverUrl = DopaBaseUrl + 'get_dopa_wdpa_lc_copernicus?agg=2&wdpaid=' + selSettings.WDPAID;

					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(copernicusLandCoverUrl,function(d){
							var dataSet = [];
							var chartSeriesData = [];
							var tableChartColors = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(data.label, data.lc_sqkm_perc_tot_sqkm, data.lc_sqkm, data.color);
								dataSet.push(newDataRow); 
								chartSeriesData.push({'name': data.label,'value': data.lc_sqkm});
								tableChartColors.push(data.color) ;
							});

							var chartData = {
								// color: tableChartColors,
								title: indicatorTitle,
								series: [{
									type: 'treemap',
									breadcrumb: {
										show: true
									},
									data: chartSeriesData,
									levels: [
										{
											color: tableChartColors,
											colorMappingBy: 'index'
										}
									]
								}],
								tooltip: {
									trigger: 'item',
									triggerOn: 'mousemove'
								}
							}
							$().createNoAxisChart(cardTwigName , chartData); 

							var columnData = [
								{ title: "Land Cover Class" },
								{ title: "% Covered" },
								{ title: "Calculated Surface (km2)" },
								{ title: "Color Map" },
							];
							var columnSettings = [
								{
									"targets": 3,
									"createdCell": function (td, cellData, rowData, row, col) {
										$(td).attr('style', 'background: '+ cellData +' !important; color: '+ cellData +';');
									},
								},
								{ "className": cardTwigName, "targets": [ "_all" ] }
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true,
								defaultSort: [[1, 'desc']],
							}
							$().createDataTable('#data-table-'+cardTwigName, tableData); 
						});
					}
				});

// ############################################## ESA Land Cover maps (1995, 2000, 2005, 2010, 2015) ##########################################
				$('#collapseCopernicusGLCOverTime').on('shown.bs.collapse', function(){
					var cardTwigName = 'CopernicusGLCOverTime'; //Set this once here as so there are no mistakes below.;
					var indicatorTitle = "Copernicus GLC Over Time for " + selSettings.paName;
					if(!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(
							'https://dopa-services.jrc.ec.europa.eu/services/d6dopa/dopa_41/get_de_wdpa_lc_esa?year=1995&agg=1&wdpaid='+selSettings.WDPAID,
							function(response){
								let results = response.records.sort((a, b) => a.label-b.label);

								let dataSet = [];
								results.forEach(r => {
									dataSet.push([
										r.label,
										r.percent,
										r.area,
										r.color
									]);
								});

								let columnData = [
									{ title: 'Land Cover Class' },
									{ title: '% Covered' },
									{ title: 'Calculated Surface (km2)' },
									{ title: 'Color Map' }
								];

								// let columnSettings = [{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": "_all" }];
								let columnSettings = [
									{
										"targets": 3,
										"createdCell": function (td, cellData, rowData, row, col) {
											$(td).attr('style', 'background: '+ cellData +' !important; color: '+ cellData +';');
										}
									},
									{"className": cardTwigName, "targets": ["_all"]}
								];
								
								let tableData = {
									title: indicatorTitle,
									columns: columnData,
									columnDefs: columnSettings,
									data: dataSet,
									attribution: dopaAttribution,
									isComplex: false
								};
								
								$().createDataTable('#data-table-'+cardTwigName, tableData); 

								// $().addCardMapLayer(cardTwigName, '&STYLES=Land_Cover_Agg_1');
								$("#action-button-CopernicusGLCOverTime_AggregationLevel1").click(function(event){
									$().addCardMapLayer(cardTwigName, '&STYLES=Land_Cover_Agg_1');
								});
								$("#action-button-CopernicusGLCOverTime_AggregationLevel2").click(function(event){
									$().addCardMapLayer(cardTwigName, '&STYLES=Land_Cover_Agg_2');
								});
								$("#action-button-CopernicusGLCOverTime_AggregationLevel3").click(function(event){
									$().addCardMapLayer(cardTwigName, '&STYLES=Land_Cover_Agg_3');
								});
							}
						);
					}
				});

// #################################################### ESA Land Cover change 1995-2015 #########################################################
				$('#collapseEsaLC').on('shown.bs.collapse', function(){
					var cardTwigName = 'EsaLC'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Natural land pattern fragmentation in " + selSettings.paName + " (1995-2020)";
					
					$.getJSON(DopaBaseUrl + "get_dopa_wdpa_lcc_esa?sortfield=lcc_sqkm&wdpaid=" + selSettings.WDPAID,
					function(response){
						
						let data = response.records.sort((a, b) => b.lcc_sqkm - a.lcc_sqkm);

						let dataKeys = {};
						let dataNodes = [];
						let dataLinks = [];
						data.forEach(d => {
							var yr1995 = d.lc1_1995.trim() + ' 1995';
							var yr2020 = d.lc1_2020.trim() + ' 2020';
							if(!dataKeys[d.lc1_1995]){
								dataKeys[d.lc1_1995] = true;
								dataNodes.push({
									name: yr1995,
									itemStyle: {
										color: getLcColor(yr1995),
									}
								});
							}

							if(!dataKeys[d.lc1_2020]){
								dataKeys[d.lc1_2020] = true;
								dataNodes.push({
									name: yr2020,
									itemStyle: {
										color: getLcColor(yr2020),
									}
								});
							}

							dataLinks.push({
								source: yr1995,
								target: yr2020,
								value: d.lcc_sqkm
							});
						});

						function getLcColor(landCoverClass){
							switch(landCoverClass) {
								case "Natural / semi-natural land 1995":
								case "Natural / semi-natural land 2020":
									return "#538135";
								case "Mosaic natural / managed land 1995":
								case "Mosaic natural / managed land 2020":
									return "#cca533";
								  case "Cultivated / managed land 1995":
								case "Cultivated / managed land 2020":
									return "#d07a41";
								  case "Water / snow and ice 1995":
								case "Water / snow and ice 2020":
									return "#536fa0";
								default:
								  break;
						  }
						}
						
						//Chart
						var chartData = {
							color: "ignore",
							title: indicatorTitle,
							tooltip: {
								trigger: 'item',
								triggerOn: 'mousemove'
							},
							series: {
								type: 'sankey',
								// layout: 'none',
								emphasis: {
									focus: 'adjacency'
								},
								data: dataNodes,
								links: dataLinks
							},
							xAxis: {
								show: false
							},
							yAxis: {
								show: false
							}
						};					
						$().createXYAxisChart(cardTwigName , chartData);
					});	
				});

// ########################################################### Neal Real-time - Fires #############################################################
				$('#collapseFires').on('shown.bs.collapse', function () {
					var cardTwigName = 'Fires'; //Set this once here as so there are no mistakes below.
					$( "div.fires_1d" ).tooltip({
							title: moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_7d" ).tooltip({
							title: moment().subtract(7, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_30d" ).tooltip({
							title: moment().subtract(30, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_90d" ).tooltip({
							title: moment().subtract(90, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					var layerArgs = moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
					$().addCardMapLayer(cardTwigName, layerArgs);
					$( "button.layer-button#layer-button-fires_1d" ).click(function( event ) {
						var layerArgs = moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_1d"); 
					});
					$( "button.layer-button#layer-button-fires_7d" ).click(function( event ) {
						var layerArgs = moment().subtract(7, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_7d"); 
					});
					$( "button.layer-button#layer-button-fires_30d" ).click(function( event ) {
						var layerArgs = moment().subtract(30, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_30d"); 
					});
					$( "button.layer-button#layer-button-fires_90d" ).click(function( event ) {
						var layerArgs = moment().subtract(90, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addCardMapLayer(cardTwigName, layerArgs, "fires_90d");  
					});
				});

// ########################################################### Neal Real-time - Floods #############################################################
				$('#collapseFloods').on('shown.bs.collapse', function () {
					var cardTwigName = 'Floods'; //Set this once here as so there are no mistakes below.
					var layerArgs = moment().format('YYYY-MM-DD');
					$().addCardMapLayer(cardTwigName, layerArgs);
				});

// ########################################################### Neal Real-time - Droughts #############################################################
				$('#collapseDroughts').on('shown.bs.collapse', function () {
					var cardTwigName = 'Droughts'; //Set this once here as so there are no mistakes below.
					var edoDay = moment().format('DD');
					var edoMonth = moment().format('MM') - 1;
					if(edoDay <=10) {edoDay = '01'}else if(edoDay >=11 && edoDay <= 20){edoDay = '11'}else if(edoDay >=21 ){edoDay = '21'}else{}
					var layerArgs = 'SELECTED_YEAR=' + moment().format('YYYY') + '&SELECTED_MONTH=' + edoMonth + '&SELECTED_TENDAYS='+edoDay;
					$().addCardMapLayer(cardTwigName, layerArgs); 
				});

// ###################################################### Species Numbers in Protected Area ############################################################
				$('#collapseSpeciesLayers').on('shown.bs.collapse', function () {
					$.getJSON(
						DopaBaseUrl + "get_dopa_wdpa_redlist_status?format=json&wdpaid=" + selSettings.WDPAID,
						function(response){
							var cardTwigName = 'SpeciesLayers'; //Set this once here as so there are no mistakes below.;
							var indicatorTitle = "Species found in " + selSettings.paName;
							if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
								var dataSet = response.records.map(function(el){
									let propVal = [];
									for(let k in el){
										propVal.push(el[k]);
									}
									return propVal;
								}); 
								var columnData = [];
								response.metadata.fields.forEach(f => {
									columnData.push({
										title: f.name.replace("_", " ")
									});
								});
								var columnSettings = [{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": "_all" }];
								var tableData = {
									title: indicatorTitle,
									columns: columnData,
									columnDefs: columnSettings,
									data: dataSet,
									attribution: dopaAttribution,
									isComplex: false,
									responsive: true
								}
								$().createDataTable('#data-table-'+cardTwigName, tableData); 
							}
						}
					);

				});

// ####################################################### Species List per Protected Area #############################################################
				$('#collapseSpeciesList').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesList'; //Set this once here as so there are no mistakes below.
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$().insertBiopamaLoader(currentTableDiv);
					var indicatorTitle = "Threatened and near threatened species (computed) for " + selSettings.paName; 
					var speciesRedListUrl = DopaBaseUrl + 'get_dopa_wdpa_redlist_list?format=json&wdpaid=' + selSettings.WDPAID;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(
							speciesRedListUrl,
								function(d){
								var dataSet = [];
								var chartSeriesData = [];
								$(d.records).each(function(i, data) {
									var newDataRow = [];
									newDataRow.push('<a href="https://www.iucnredlist.org/search?query='+data.id_no+'&searchType=species"target="_blank">'+data.id_no+'</a>', data.binomial, data.phylum, data.class, data.order, data.family, data.category);
									dataSet.push(newDataRow); 
								});
								var columnData = [
									{ title: "IUCN Red List ID" },
									{ title: "Scientific Name" },
									{ title: "Phylum" },
									{ title: "Class" },
									{ title: "Order" },
									{ title: "Family" },
									{ title: "Status" }
								];
								var columnSettings = [
									{
										"targets": 6,
										"className": "dt-center",
										"createdCell": function (td, cellData, rowData, row, col) { 
											switch(cellData) {
												case "EX":
													$(td).html('<div class="species-table-status species-ex">Extinct (EX)</div>');
													break;
												case "EW":
													$(td).html('<div class="species-table-status species-ew">Extinct in the Wild (EW)</div>');
													break;
												case "CR":
													$(td).html('<div class="species-table-status species-cr">Critically Endangered (CR)</div>');
													break;
												case "EN":
													$(td).html('<div class="species-table-status species-en">Endangered (EN)</div>');
													break;
												case "VU":
													$(td).html('<div class="species-table-status species-vu">Vulnerable (VU)</div>');
													break;
												case "NT":
													$(td).html('<div class="species-table-status species-nt">Near Threatened (NT)</div>');
													break;
												case "LC":
													$(td).html('<div class="species-table-status species-lc">Least Concern (LC)</div>');
													break;
												default:
													$(td).html('<div class="species-table-status species-dd">Data Deficient (DD)</div>');
											}
										},
									},
									{ "className": cardTwigName, "targets": [ "_all" ] }
								];
								var tableData = {
									title: indicatorTitle,
									columns: columnData,
									columnDefs: columnSettings,
									data: dataSet,
									attribution: dopaAttribution,
									isComplex: true
								}
								$().createDataTable('#data-table-'+cardTwigName, tableData); 
							}
						);
					}
				});

// ############################################### Species occurrences reported to the GBIF #########################################################
				$('#collapseSpeciesGbif').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesGbif'; //Set this once here as so there are no mistakes below.
					//
				});
				$('#collapseSpeciesGbif').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesGbif'; //Set this once here as so there are no mistakes below.
					//
				});
		    });
		}
    };
})(jQuery, Drupal);
