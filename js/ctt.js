(function ($, Drupal) {


    $(".ctt-card-title").on( 'click', function(e) {
        if ($(e.currentTarget).hasClass('collapsed')){
            closeIndicatorCard();
        } else {
            var nodeID = $(this).next().find("div.ctt-nid").text().trim();
            $().insertBiopamaLoader("#view-wrapper-"+nodeID); 
            currentIndicatorNodeURL = '/indicator_card/'+nodeID;
            showIndicatorCard(currentIndicatorNodeURL, nodeID);
        }

    });

    function showIndicatorCard(indicatorURL, nodeID){
        $('.wrapper-ctt-data-card > div.ctt-wrapper').empty();//acctually delete the indicator card as it will break the detection of the other CTT indicator variables if it stays.
        // Drupal.ajax({ 
        //     url: indicatorURL,
        //     success: function(response) {			
        //         var $countryDialogContents
        //         for (var key in response) {
        //             // skip loop if the property is from prototype
        //             if (!response.hasOwnProperty(key)) continue;
        //             var obj = response[key];
        //             for (var prop in obj) {
        //                 // skip loop if the property is from prototype
        //                 if(!obj.hasOwnProperty(prop)) continue;
        //                 //console.log(prop + " = " + obj[prop]);
        //                 if(prop == "data"){
        //                     //console.log(prop + " = " + obj[prop]);
        //                     //$('#block-indicatorcard').show();
        //                     $countryDialogContents = $('<div>' + response[key].data + '</div>').appendTo('body');
        //                 }
        //             }
        //         }
        //         $().removeBiopamaLoader("#view-wrapper-"+nodeID); 
        //         $("#view-wrapper-"+nodeID).empty();
        //         $countryDialogContents.appendTo("#view-wrapper-"+nodeID);
        //         Drupal.attachBehaviors($('.wrapper-ctt-data-card > div.ctt-wrapper').get(0));
        //     }
        // }).execute(); 

        var ajaxSettings = {
            url: indicatorURL,
            wrapper: "view-wrapper-"+nodeID,
            method: 'append',
        };
        var myAjaxObject = Drupal.ajax(ajaxSettings);
        myAjaxObject.execute();
        Drupal.attachBehaviors($('.wrapper-ctt-data-card > div.ctt-wrapper:visible').get(0));


    }
})(jQuery, Drupal);