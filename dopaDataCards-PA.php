<?php

/* 
Anatomy of a Data Card
'cardId' => [ required to identify and use the card in JS
  'name' => '',
  'description' => '', 
  'descriptionLong' => '', //not really used, but it could be!
  'customTop' => '', //Puts a generic div at the top for more custom content - for injecting via JS
  'chart' => BOOL,
  'table' => BOOL, 
  'layers' => [[
    'name' => '', //title of the layer
	'id' => '', //ID to uniquely manage this layer in the map !Do not include an ID if there is no layer! 
	'buttonTitle' => '', //if there's more then one layer in the array this will be in the title of the generated button
	'type' => '', //Raster or Vector
	'scheme' => '', //if raster - xyz, tms... other? 
	'url' => '', //the URL - it it needs an argument, that's managed in JS
	'customArgs' => BOOL, //setting this to true will prevent the layer from being rendered normally. Instead it will need to be called specifically so we can attach the custom arguments.
	'description' => '', 
	'legend' => [
	  'type' => 'choropleth', //only choropleth or gradient
	  'classes' => [
	  	[
		  'color' => '#fff', //supports any color type (hex, rgb, etc)
		  'label' => '', //it's what is written under the corresponding color 
		  'class' => 'fires_1d', //optional - todo add interaction
		]
	  ]
	], 
  ]],
  'customBottom' => '', //Puts a generic div at the bottom for more custom content - for injecting via JS
  'link' => '', //!mandatory
  'linkIcon' => '', //todo - add optional icon for reference link
  'linkText' => '', //if null, it will display default text
],
 */

$DataCardsClimate = [
    // 'ElevationProfile' => [
	// 	'name' => 'Elevation profile',
	// 	'description' => 'Virtual elevation profile of the Protected Area providing minimum, maximum, median, mean elevation values in meters.',
	// 	'chart' => TRUE,
	// 	'table' => TRUE, 
	// 	'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Elevation', 
    // ],

	'AverageClimate' => [
		'name' => 'Average climate',
		'description' => 'Climate statistics providing monthly rainfall averages (mm) for the terrestrial protected areas and monthly mean, maximum and minimum temperatures (C°) for the land and/or the sea surface of the protected area.',
		'chart' => TRUE,
		'table' => FALSE, 
		'link' => 'https://dopa.jrc.ec.europa.eu/static/dopa/static/dopa/files/factsheets/en/DOPA%20Factsheet%20F1%20EN%20Climate%20and%20Elevation.pdf', 
    ]
];

$DataCardsEcosystemFunctioningAndServices = [
    'BelowGroundCarbon' => [
		'name' => 'Below ground carbon',
		'description' => 'Protected area statistics for the amount of below ground carbon', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Below Ground Carbon',
			'id' => 'bgCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:below_ground_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/africa_platform:bgb_2021@EPSG:900913@jpg/{z}/{x}/{y}.png',
			'description' => NULL,
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '0 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>8,000 Mg',
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/static/dopa/static/dopa/files/factsheets/en/DOPA%20Factsheet%20J3%20EN%20Below%20Ground%20Carbon.pdf', 
		'linkText' => NULL, 
    ],

	'SoilOrganicCarbon' => [
		'name' => 'Soil organic carbon',
		'description' => 'Protected area statistics for the amount of soil organic carbon (0-30 cm depth)',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Global Soil Organic Carbon (GSOC)',
			'id' => 'GSOCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:soil_organic_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:Soil_organic_carbon@EPSG:900913@jpg/{z}/{x}/{y}.png',
			'description' => 'The global soil organic carbon concentration map provides users with essential information on degraded areas and soil fertility as well as on the contribution to carbon storage mitigating climate change.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '0 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>25,000 Mg',
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/static/dopa/static/dopa/files/factsheets/en/DOPA%20Factsheet%20J1%20EN%20Soil%20Carbon.pdf', 
		'linkText' => NULL, 
    ],

	'AboveGroundCarbon' => [
		'name' => 'Above ground carbon',
		'description' => 'Protected area statistics for above ground carbon.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Global Above Ground Carbon',
			'id' => 'AGCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:above_ground_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/africa_platform:agb_2021@EPSG:900913@jpg/{z}/{x}/{y}.png',
			'description' => 'The above-ground carbon (AGC) layer is expressed in Mg (megagrams or tonnes) of biomass per km<sup>2</sup>.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '0 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>25,000 Mg',
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/static/dopa/static/dopa/files/factsheets/en/DOPA%20Factsheet%20J1%20EN%20Soil%20Carbon.pdf', 
		'linkText' => NULL,
    ],
];

$DataCardsFunding = [ 
	'eConservation' => [ 
		'name' => 'eConservation',
		'description' => 'Map of conservation projects funded by the EU (Life and BEST programmes, EuropeAid) and the World Bank. See our other tool <a href="http://econservation.jrc.ec.europa.eu/" target="_blank">eConservation</a> for more details.',  
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'eConservation', 
			'id' => 'eConservation', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/econservation/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=econservation%3Amt_190_site_project_country_wdpa&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG%3A3857&BBOX={bbox-epsg-3857}', 
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#7fad41', 
						'label' => 'Inside PAs',
					],
					[
						'color' => '#004494', 
						'label' => 'Outside PAs',
					],
				], 
			],
		]], 
		'link' => 'http://econservation.jrc.ec.europa.eu/', 
		'linkText' => 'Get more info about eConservation', 
    ],
];

$DataCardsHabitatsAndEcosystems = [
    'EcoregionsOverlappingProtectedAreas' => [
		'name' => 'Ecoregions overlapping with the protected area',
		'description' => 'Marine and terrestrial ecoregions overlapping with the protected area.', 
		'buttonGroup' => NULL, 
		'chart' => NULL,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Ecoregions overlapping with the protected area',
			'description' => NULL,
			'id' => 'EcoregionsOverlappingProtectedAreas', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			//'url' => 'https://proxy.biopama.org/globalfloods-ows.ecmwf.int/glofas-ows/ows.py?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.3.0&request=GetMap&crs=EPSG:3857&transparent=true&width=256&height=256&layers=FloodHazard100y&zIndex=33&opacity=1&time=', //YYYY-MM-DD
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:dopa_geoserver_ecoregions_master_201905&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}', //YYYY-MM-DD
			'description' => 'Marine and terrestrial ecoregions overlapping with the protected area.', 
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#7fad41', 
						'label' => 'On target', 
					],
					[
						'color' => '#b3b536', 
						'label' => '-1%',
					],
					[
						'color' => '#b4b536', 
						'label' => '-2%',
					],
					[
						'color' => '#e49827', 
						'label' => '-5%',
					],[
						'color' => '#e46327', 
						'label' => '-10%',
					],[
						'color' => '#fd1300', 
						'label' => '-17%',
					],
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/dopa/documentation/en', 
    ],

	'ForestCover' => [
		'name' => 'Forest loss 2000-2021',
		'description' => 'Forest loss (2000-2021) in this protected area. The chart shows the percentage of decrease of tree cover since 2000 in this protected area. In the table the tree cover (2000) and the tree cover loss (2000-2021) are expressed in sqkm.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Forest Loss',
			'id' => 'ForestLossGain', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://storage.googleapis.com/earthenginepartners-hansen/tiles/gfc_v1.4/loss_alpha/{z}/{x}/{y}.png', 
			'description' => 'Forest loss layer as derived from a global remote sensing product based on Landsat data. See <a href="http://www.globalforestwatch.org/" target="_blank">http://www.globalforestwatch.org/</a> for more details',
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => 'red', 
						'label' => 'Forest Loss',
					]
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Forest+Cover', 
    ],

	'InlandSurfaceWater' => [ 
		'name' => 'Inland surface water',
		'description' => 'Areas of permanent and seasonal surface inland water and their changes over time (1984 - 2018) are expressed in km<sup>2</sup> and percentages.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Water Occurrence (1984-2018)',
			'id' => 'waterOccurrence', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://storage.googleapis.com/global-surface-water/tiles2020/occurrence/{z}/{x}/{y}.png', 
			// 'url' => 'https://proxy.biopama.org/jeodpp.jrc.ec.europa.eu/jeodpp/services/ows/ts/hydrography/gsw-jrc?z={z}&x={z}&y={y}&format=png&layers=HY.GSW.Occurrence',
			'description' => 'The Water Occurrence dataset shows where surface water occurred between 1984 and 2018 and provides information concerning overall water dynamics. This product captures both the intra and inter-annual variability and changes. The occurrence is a measurement of the water presence frequency (expressed as a percentage of the available observations over time actually identified as water). The provided occurrence accommodates for variations in data acquisition over time (i.e. temporal deepness and frequency density of the satellite observations) in order to provide a consistent characterization of the water dynamic over time.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#fff', 
						'label' => 'Sometimes Water ( >0% )',
					],
					[
						'color' => '#bf7fbf', 
						'label' => '',
					],
					[
						'color' => 'blue', 
						'label' => 'Always Water ( 100% )',
					],
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=water', 
    ],
	'HabitatDiversity' => [ 
		'name' => 'Habitat diversity',
		'description' => 'The habitat diversity of the protected area has been characterized by the number of distinct habitats for the terrestrial part, and by looking at the complexity of the bathymetry for the marine parts.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Habitat diversity',
			'id' => 'habitatDiversity', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:segment_layers_202101&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}'
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Habitat', 
    ],
	'LandDegradation' => [
		'name' => 'Land degradation',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Land Productivity', 
			'id' => 'LandProductivity', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3%3ALPD&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG%3A3857&BBOX={bbox-epsg-3857}', 
			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:LPD@EPSG:900913@jpg/{z}/{x}/{y}.png',
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#ab2828', 
						'label' => 'Persistent severe decline in productivity',
					],
					[
						'color' => '#ed7325', 
						'label' => 'Persistent moderate decline in productivity',
					],
					[
						'color' => '#ffd954', 
						'label' => 'Stable, but stressed',
					],
					[
						'color' => '#b8d879', 
						'label' => 'Stable productivity',
					],
					[
						'color' => '#46a246', 
						'label' => 'Persistent increase in productivity',
					],
				], 
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Productivity', 
    ], 
	'LandFragmentation' => [
		'name' => 'Land fragmentation',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Land Fragmentation',
			'id' => 'LandFragmentation', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3%3Aland_fragmentation&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG%3A3857&BBOX={bbox-epsg-3857}&TIME=1995-01-01', 
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#7fad41', 
						'label' => 'Core',
					],
					[
						'color' => '#878787', 
						'label' => 'Non Natural',
					],
					[
						'color' => '#52781f', 
						'label' => 'Edge',
					],
					[
						'color' => '#f1c200', 
						'label' => 'Core Perforation',
					],
					[
						'color' => '#e89717', 
						'label' => 'Islet',
					],
					[
						'color' => '#d56317', 
						'label' => 'Linear',
					],
				], 
			],
		]], 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Fragmentation', 
    ],
];

$DataCardsHumanImpactAndPressures = [
    'AgriculturalPressure' => [
		'name' => 'Agricultural Pressure',
		'description' => 'Percentage of the surface of this protected area and of its 10 km unprotected buffer covered by cropland.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [
			[
				'name' => 'Copernicus Global Land Cover Map (2015)',
				'description' => 'The cropland mapped is derived from the class “Cultivated and managed vegetation/agriculture (cropland)” of the Copernicus 100 m Land Cover Map for the year 2015.',
				'id' => 'AgriculturalPressure', 
				'type' => 'raster', 
				'scheme' => 'xyz',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_copernicus_2018&STYLES=cropland_lc_copernicus&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=34&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#eca90c'
							// 'label' => 'On target', 
						]
					],
				],
			]
		],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Agricultural+Pressure'
    ],

	'RoadPressure' => [
		'name' => 'Road Pressure',
		'description' => 'Percentage of the surface of this protected area and of its 10 km unprotected buffer with presence of roads (roads have been buffered by 250 m to calculate this percentage).', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [
			[
				'name' => 'The Global Roads Open Access Data Set, Version 1 (gROADSv1)',
				'description' => 'The data set combines the best available roads data by country into a global roads coverage, using the UN Spatial Data Infrastructure Transport (UNSDI-T) version 2 as a common data model. All country road networks have been joined topologically at the borders, and many countries have been edited for internal topology.',
				'id' => 'RoadPressure', 
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:g_roads@EPSG:900913@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:g_roads&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#919ca2'
						]
					],
				],
			]
		],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Road+Pressure'
    ],

	'PopulationPressure' => [
		'name' => 'Population Pressure',
		'description' => 'Population (2015) and population change (2000-2015) pressures for this protected area and its 10 km unprotected buffer.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [
			[
				'name' => 'GHS Population Grid',
				'description' => 'Distribution and density of population, expressed as the number of people per km2',
				'id' => 'PopulationPressure75',
				'buttonTitle' => '1975',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:g_roads@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_population_75&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',				
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#fcae72'
						],
						[
							'color' => '#b30000',
							'label' => '500'
						]
					],
				],
			],
			[
				'name' => 'GHS Population Grid',
				'description' => 'Distribution and density of population, expressed as the number of people per km2',
				'id' => 'PopulationPressure90', 
				'buttonTitle' => '1990',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:g_roads@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_population_90&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',				
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#fcae72'
						],
						[
							'color' => '#b30000',
							'label' => '500'
						]
					],
				],
			],
			[
				'name' => 'GHS Population Grid',
				'description' => 'Distribution and density of population, expressed as the number of people per km2',
				'id' => 'PopulationPressure00', 
				'buttonTitle' => '2000',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:g_roads@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_population_00&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',				
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#fcae72'
						],
						[
							'color' => '#b30000',
							'label' => '500'
						]
					],
				],
			],
			[
				'name' => 'GHS Population Grid',
				'description' => 'Distribution and density of population, expressed as the number of people per km2',
				'id' => 'PopulationPressure15',
				'buttonTitle' => '2015',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:g_roads@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_population_15&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',				
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#fcae72'
						],
						[
							'color' => '#b30000',
							'label' => '500'
						]
					],
				],
			]
		],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Population+Pressure'
    ],

	'BuiltupAreasPressure' => [
		'name' => 'Built-up Areas Pressure',
		'description' => 'Surface of this protected area and of its 10 km unprotected buffer that is covered by constructions, expressed both as a built-up area (km2) and as a percentage.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [
			[
				'name' => 'GHS Built-Up Grid',
				'description' => 'Built-up presence, values are expressed as decimals (Float) from 0 to 100',
				'id' => 'BuiltupAreasPressure75', 
				'buttonTitle' => '1975',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:ghsl_builtup_14@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_builtup_75&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#d7b5d8'
						],
						[
							'color' => '#980043',
							'label' => '1'
						]
					],
				],
			],
			[
				'name' => 'GHS Built-Up Grid',
				'description' => 'Built-up presence, values are expressed as decimals (Float) from 0 to 100',
				'id' => 'BuiltupAreasPressure90', 
				'buttonTitle' => '1990',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:ghsl_builtup_14@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_builtup_90&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#d7b5d8'
						],
						[
							'color' => '#980043',
							'label' => '1'
						]
					],
				],
			],
			[
				'name' => 'GHS Built-Up Grid',
				'description' => 'Built-up presence, values are expressed as decimals (Float) from 0 to 100',
				'id' => 'BuiltupAreasPressure00', 
				'buttonTitle' => '2000',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:ghsl_builtup_14@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_builtup_00&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#d7b5d8'
						],
						[
							'color' => '#980043',
							'label' => '1'
						]
					],
				],
			],
			[
				'name' => 'GHS Built-Up Grid',
				'description' => 'Built-up presence, values are expressed as decimals (Float) from 0 to 100',
				'id' => 'BuiltupAreasPressure14', 
				'buttonTitle' => '2014',
				'type' => 'raster', 
				'scheme' => 'xyz',
				// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:ghsl_builtup_14@EPSG:3857@jpg/{z}/{x}/{y}.png',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:ghsl_builtup_14&STYLES=&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&TILED=true&TILESORIGIN=-180,90&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#ffffff',
							'label' => '0'
						],
						[
							'color' => '#d7b5d8'
						],
						[
							'color' => '#980043',
							'label' => '1'
						]
					],
				],
			]
		],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Built+up+pressure'
    ],

	'AnthropogenicPressure' => [
		'name' => 'Overview of normalised anthropogenic pressures',
		'description' => 'Country normalised pressure from population, built-up areas, roads and agriculture on this protected area and its surroundings.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => FALSE, 
		// 'layers' => [
		// 	[
		// 		'name' => 'GHS Built-Up Grid',
		// 		'description' => 'Built-up presence, values are expressed as decimals (Float) from 0 to 100',
		// 		'id' => 'BuiltupAreasPressure', 
		// 		'type' => 'raster', 
		// 		'scheme' => 'xyz',
		// 		'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:ghsl_builtup_14@EPSG:3857@jpg/{z}/{x}/{y}.png',
		// 		'legend' => [
		// 			'type' => 'gradient',
		// 			'classes' => [
		// 				[
		// 					'color' => '#ffffff',
		// 					'label' => '0'
		// 				],
		// 				[
		// 					'color' => '#d7b5d8'
		// 				],
		// 				[
		// 					'color' => '#980043',
		// 					'label' => '1'
		// 				]
		// 			],
		// 		],
		// 	]
		// ],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Pressure'
    ]
];

$DataCardsLandCover = [
    'CopernicusGLC' => [
		'name' => 'Copernicus Global Land Cover 2019',
		'description' => 'Using the second aggregation level, the land cover classes are provided for this country for the year 2019 km<sup>2</sup> and %.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Copernicus Global Land Cover 2019',
			'id' => 'CopernicusGLC', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:PROBAV_LC100@EPSG:900913@png/{z}/{x}/{y}.png', 
			'description' => NULL,
			'legend' => NULL
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover',
    ],


	// 'CopernicusGLCOverTime' => [
	// 	'name' => 'ESA Land Cover maps (1995, 2000, 2005, 2010, 2015)',
	// 	'description' => 'Using three different aggregation levels, the land cover classes are provided for this protected area for the years 1995, 2000, 2005, 2010 and 2015 in km<sup>2</sup> and %.', 
	// 	'buttonGroup' => [
	// 		[
	// 			'id' => 'CopernicusGLCOverTime_AggregationLevel1',
	// 			'title' => 'Aggregation level 1',
	// 			'customData' => 'data-qs-param="STYLES=Land_Cover_Agg_1"'
	// 		],
	// 		[
	// 			'id' => 'CopernicusGLCOverTime_AggregationLevel2',
	// 			'title' => 'Aggregation level 2',
	// 			'customData' => 'data-qs-param="STYLES=Land_Cover_Agg_2"'
	// 		],
	// 		[
	// 			'id' => 'CopernicusGLCOverTime_AggregationLevel3',
	// 			'title' => 'Aggregation level 3',
	// 			'customData' => 'data-qs-param="STYLES=Land_Cover_Agg_3"'
	// 		]
	// 	],
	// 	'chart' => TRUE,
	// 	'table' => TRUE, 
	// 	'layers' => [
	// 		[
	// 			'name' => 'ESA Land Cover maps (1995, 2000, 2005, 2010, 2015)',
	// 			'id' => 'CopernicusGLCOverTime95', 
	// 			'buttonTitle' => '1995',
	// 			'type' => 'raster', 
	// 			'scheme' => 'tms',
	// 			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:PROBAV_LC100@EPSG:900913@png/{z}/{x}/{y}.png', 
	// 			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_mosaic&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}&TIME=1995-01-01',
	// 			'description' => NULL,
	// 			'legend' => NULL
	// 		],
	// 		[
	// 			'name' => 'ESA Land Cover maps (1995, 2000, 2005, 2010, 2015)',
	// 			'id' => 'CopernicusGLCOverTime00', 
	// 			'buttonTitle' => '2000',
	// 			'type' => 'raster', 
	// 			'scheme' => 'tms',
	// 			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:PROBAV_LC100@EPSG:900913@png/{z}/{x}/{y}.png', 
	// 			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_mosaic&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}&TIME=2000-01-01',
	// 			'description' => NULL,
	// 			'legend' => NULL
	// 		],
	// 		[
	// 			'name' => 'ESA Land Cover maps (1995, 2000, 2005, 2010, 2015)',
	// 			'id' => 'CopernicusGLCOverTime05', 
	// 			'buttonTitle' => '2005',
	// 			'type' => 'raster', 
	// 			'scheme' => 'tms',
	// 			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:PROBAV_LC100@EPSG:900913@png/{z}/{x}/{y}.png', 
	// 			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_mosaic&STYLES=Land_Cover_Agg_1&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}&TIME=2005-01-01',
	// 			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_mosaic&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}&TIME=2005-01-01',
	// 			'description' => NULL,
	// 			'legend' => NULL
	// 		],
	// 		[
	// 			'name' => 'ESA Land Cover maps (1995, 2000, 2005, 2010, 2015)',
	// 			'id' => 'CopernicusGLCOverTime10', 
	// 			'buttonTitle' => '2010',
	// 			'type' => 'raster', 
	// 			'scheme' => 'tms',
	// 			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:PROBAV_LC100@EPSG:900913@png/{z}/{x}/{y}.png', 
	// 			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_mosaic&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}&TIME=2010-01-01',
	// 			'description' => NULL,
	// 			'legend' => NULL
	// 		],
	// 		[
	// 			'name' => 'ESA Land Cover maps (1995, 2000, 2005, 2010, 2015)',
	// 			'id' => 'CopernicusGLCOverTime15', 
	// 			'buttonTitle' => '2015',
	// 			'type' => 'raster', 
	// 			'scheme' => 'tms',
	// 			// 'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:PROBAV_LC100@EPSG:900913@png/{z}/{x}/{y}.png', 
	// 			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3:land_cover_mosaic&FORMAT=image/png&TRANSPARENT=true&HEIGHT=256&WIDTH=256&ZINDEX=33&SRS=EPSG:3857&BBOX={bbox-epsg-3857}&TIME=2015-01-01',
	// 			'description' => NULL,
	// 			'legend' => NULL
	// 		]
	// 	],
	// 	'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover',
    // ],
	
	'EsaLC' => [
		'name' => 'ESA Land Cover change 1995-2020',
		'description' => 'The land cover class change for this protected area from the years 1995 to 2020 in km<sup>2</sup>', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => FALSE, 
		'layers' => [[
			'name' => 'Land Cover Change (1995 to 2020)',
			'id' => 'EsaLC', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:LCC_1995_2015@EPSG:900913@jpg/{z}/{x}/{y}.png',
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#ff7f1f', 
						'label' => 'Natural / semi-natural land → Mosaic natural / managed land',
					],[
						'color' => '#e50003', 
						'label' => 'Natural / semi-natural land → Cultivated / managed land',
					],[
						'color' => '#3e86f3', 
						'label' => 'Natural / semi-natural land → Water / snow and ice',
					],[
						'color' => '#3cc943', 
						'label' => 'Mosaic natural / managed land → Natural / semi-natural land',
					],[
						'color' => '#fe6d70', 
						'label' => 'Mosaic natural / managed land → Cultivated / managed land',
					],[
						'color' => '#3fd6ff', 
						'label' => 'Mosaic natural / managed land → Water / snow and ice',
					],[
						'color' => '#438c28', 
						'label' => 'Cultivated / managed land → Natural / semi-natural land',
					],[
						'color' => '#c39a22', 
						'label' => 'Cultivated / managed land → Mosaic natural / managed land',
					],[
						'color' => '#00dda8', 
						'label' => 'Cultivated / managed land → Water / snow and ice',
					],[
						'color' => '#7fda95', 
						'label' => 'Water / snow and ice → Natural / semi-natural land',
					],[
						'color' => '#f4ff74', 
						'label' => 'Water / snow and ice → Mosaic natural / managed land',
					],[
						'color' => '#ffcd29', 
						'label' => 'Water / snow and ice → Cultivated / managed land', 
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover', 
		'linkText' => NULL, 
    ],
];

$DataCardsNearRealTime = [ 
    'Fires' => [
		'name' => 'Fires',
		'description' => 'Active fire products from the Moderate Resolution Imaging Spectroradiometer (MODIS) for the last 24 hours up to the last 90 days.', 
		'layers' => [[
			'name' => 'Active Fires',
			'buttonTitle' => '1 day',
			'id' => 'fires_1d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/firms.modaps.eosdis.nasa.gov/mapserver/wms/fires/72037f6c91f35a1ac2885bfb42fafd9f?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=fires_modis_24&zIndex=72&opacity=1&time=',
			'customArgs' => TRUE,
			// 'description' => 'Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or "hot spot".', 
			'description' => "DOPA Explorer is the Joint Research Centre’s web based information system on the world's protected areas, which helps the European Commission and other users to assess the state of and the pressure on protected areas at multiple scales.Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or \"hot spot.\" Global Wildfire Information System (GWIS) uses the active fire detections provided by the NASA FIRMS (Fire Information for Resource Management System).",
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					[
						'color' => '#f00', 
						'label' => 'Last 1 Day',
						'class' => 'fires_1d',
					]
				],
			],
		],
		[
			'name' => 'Active Fires',
			'buttonTitle' => '7 days',
			'id' => 'fires_7d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/firms.modaps.eosdis.nasa.gov/mapserver/wms/fires/72037f6c91f35a1ac2885bfb42fafd9f?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=fires_modis_24&zIndex=72&opacity=1&time=',
			'customArgs' => TRUE,
			'description' => "DOPA Explorer is the Joint Research Centre’s web based information system on the world's protected areas, which helps the European Commission and other users to assess the state of and the pressure on protected areas at multiple scales.Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or \"hot spot.\" Global Wildfire Information System (GWIS) uses the active fire detections provided by the NASA FIRMS (Fire Information for Resource Management System).",
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					// [
					// 	'color' => '#f00', 
					// 	'label' => 'Last 1 Day',
					// 	'class' => 'fires_1d',
					// ],
					[
						// 'color' => '#ff7f00', 
						'color' => '#f00', 
						'label' => 'Last 7 Days',
						'class' => 'fires_7d',
					]
				],
			],
		],
		[
			'name' => 'Active Fires',
			'buttonTitle' => '30 days',
			'id' => 'fires_30d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/firms.modaps.eosdis.nasa.gov/mapserver/wms/fires/72037f6c91f35a1ac2885bfb42fafd9f?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=fires_modis_24&zIndex=72&opacity=1&time=',
			'customArgs' => TRUE,
			'description' => "DOPA Explorer is the Joint Research Centre’s web based information system on the world's protected areas, which helps the European Commission and other users to assess the state of and the pressure on protected areas at multiple scales.Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or \"hot spot.\" Global Wildfire Information System (GWIS) uses the active fire detections provided by the NASA FIRMS (Fire Information for Resource Management System).",
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					// [
					// 	'color' => '#f00', 
					// 	'label' => 'Last 1 Day',
					// 	'class' => 'fires_1d',
					// ],
					// [
					// 	'color' => '#ff7f00', 
					// 	'label' => 'Last 7 Days',
					// 	'class' => 'fires_7d',
					// ],
					[
						// 'color' => '#1e90ff', 
						'color' => '#f00', 
						'label' => 'Last 30 Days',
						'class' => 'fires_30d',
					]
				],
			],
		],
		[
			'name' => 'Active Fires',
			'buttonTitle' => '90 days',
			'id' => 'fires_90d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/firms.modaps.eosdis.nasa.gov/mapserver/wms/fires/72037f6c91f35a1ac2885bfb42fafd9f?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=fires_modis_24&zIndex=72&opacity=1&time=',
			'customArgs' => TRUE,
			'description' => "DOPA Explorer is the Joint Research Centre’s web based information system on the world's protected areas, which helps the European Commission and other users to assess the state of and the pressure on protected areas at multiple scales.Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or \"hot spot.\" Global Wildfire Information System (GWIS) uses the active fire detections provided by the NASA FIRMS (Fire Information for Resource Management System).",
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					// [
					// 	'color' => '#f00', 
					// 	'label' => 'Last 1 Day',
					// 	'class' => 'fires_1d',
					// ],
					// [
					// 	'color' => '#ff7f00', 
					// 	'label' => 'Last 7 Days',
					// 	'class' => 'fires_7d',
					// ],
					// [
					// 	'color' => '#1e90ff', 
					// 	'label' => 'Last 30 Days',
					// 	'class' => 'fires_30d',
					// ],
					[
						// 'color' => '#588d0b', 
						'color' => '#f00', 
						'label' => 'Last 90 Days',
						'class' => 'fires_90d',
					],
				],
			],
		]],
		'link' => 'https://earthdata.nasa.gov/earth-observation-data/near-real-time/firms/about-firms', 
		'linkText' => 'Get more info about Active Fires', 
    ],
    'Floods' => [ 
		'name' => 'Floods',
		'description' => 'Global historical and current flood events derived from news, governmental, instrumental, and remote sensing sources from the Dartmouth Flood Observatory and Flood hazard 100 year return period Layer from <a href="http://www.globalfloods.eu/">Global Flood Awareness System</a>',  
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'Flood hazard 100 year return period',
			'id' => 'floods', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/globalfloods-ows.ecmwf.int/glofas-ows/ows.py?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.3.0&request=GetMap&crs=EPSG:3857&transparent=true&width=256&height=256&layers=FloodHazard100y&zIndex=33&opacity=1&time=', //YYYY-MM-DD
			'description' => 'Inundated areas for flood events with a return period of 100 years, based on GloFAS climatology. Permanent water bodies derived from the Global Lakes and Wetlands Database and from the Natural Earth lakes map (<a href="http://www.naturalearthdata.com" target="_blank">naturalearthdata.com</a>).', 
			'legend' => [
				'type' => 'choropleth',
				'classes' => [	  
					[
						'color' => '#b8dbff', 
						'label' => 'Shallow (less than 1m)',
						'class' => 'floods_ne_05m',
					],
					[
						'color' => '#99ccff', 
						'label' => 'Moderate (between 1 and 3 m)',
						'class' => 'floods_ne_1m',
					],
					[
						'color' => '#6699ff', 
						'label' => 'Deep (between 3 and 10 m)',
						'class' => 'floods_ne_10m',
					],
					[
						'color' => '#3366ff', 
						'label' => 'Very deep (permanent water)',
						'class' => 'floods_ne_05m',
					],
				],
			],
		]], 
		'link' => 'http://floodobservatory.colorado.edu/', 
		'linkText' => 'Get more info about Flood Events', 
    ],
    'Droughts' => [
		'name' => 'Droughts',
		'description' => 'The indicator shows the risk of having impacts from a drought, by taking into account the exposure and socio-economic vulnerability of the area, with particular focus on the agricultural impacts.', 
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'Risk of Drought Impact',
			'id' => 'drought', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/edo.jrc.ec.europa.eu/gdo/php/wms.php?bbox={bbox-epsg-3857}&width=256&height=256&SRS=EPSG%3A3857&LAYERS=RDrI-Agri&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&ZINDEX=33&', //SELECTED_YEAR=YYY&SELECTED_MONTH=MM&SELECTED_TENDAYS=21
			'description' => 'The indicator shows the risk of having impacts from a drought, by taking into account the exposure and socio-economic vulnerability of the area, with particular focus on the agricultural impacts. Formerly known as Likelihood of Drought Impact (LDI), it differs from the latter in that soil moisture anomaly is now included and updated every ten days (dekad).',
			'legend' => [
			'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#feea08', 
						'label' => 'Low',
						'class' => 'edo_10m',
					],
					[
						'color' => '#ff7f00', 
						'label' => 'Medium',
						'class' => 'edo_1m',
					],
					[
						'color' => '#f00', 
						'label' => 'High',
						'class' => 'edo_05m',
					],
				],
			],
		]],  
		'link' => 'http://edo.jrc.ec.europa.eu/gdo/php/index.php?id=2000', 
		'linkText' => "Get more info about Risk of Droughts", 
    ]
];

$DataCardsSpecies = [
    'SpeciesLayers' => [
		'name' => 'Species numbers in protected area',
		'description' => 'The following species numbers are computed from the species ranges recorded in the IUCN Red List of Threatened Species.',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [
			[
				'name' => 'Amphibian Species Richness',
				'buttonTitle' => 'Amphibians',
				'id' => 'AmphibianSpeciesRichness', 
				'type' => 'raster', 
				'scheme' => 'tms',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:amphibians_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
				'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
	Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#d7191c', 
							'label' => 'Low richness',
						],[
							'color' => '#feedaa', 
							'label' => '',
						],[
							'color' => '#2b83ba', 
							'label' => 'High richness',
						]
					],
				],
			],
			[
				'name' => 'Bird Species Richness',
				'buttonTitle' => 'Birds',
				'id' => 'BirdSpeciesRichness', 
				'type' => 'raster', 
				'scheme' => 'tms',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:birds_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
				'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
	Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#d7191c', 
							'label' => 'Low richness',
						],[
							'color' => '#feedaa', 
							'label' => '',
						],[
							'color' => '#2b83ba', 
							'label' => 'High richness',
						]
					],
				],
			],
			[
				'name' => 'Mammal Species Richness',
				'buttonTitle' => 'Mammals',
				'id' => 'MammalSpeciesRichness', 
				'type' => 'raster', 
				'scheme' => 'tms',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:mammals_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
				'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
	Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#d7191c', 
							'label' => 'Low richness',
						],[
							'color' => '#feedaa', 
							'label' => '',
						],[
							'color' => '#2b83ba', 
							'label' => 'High richness',
						]
					],
				],
			],
			[
				'name' => 'Shark and Rays Species Richness',
				'buttonTitle' => 'Sharks and Rays',
				'id' => 'SharksSpeciesRichness', 
				'type' => 'raster', 
				'scheme' => 'tms',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:sharks_rays_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
				'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
	Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
				'legend' => [
					'type' => 'gradient',
					'classes' => [
						[
							'color' => '#d7191c', 
							'label' => 'Low richness',
						],[
							'color' => '#feedaa', 
							'label' => '',
						],[
							'color' => '#2b83ba', 
							'label' => 'High richness',
						]
					],
				],
			],
			[
				'name' => 'Coral Species Richness',
				'buttonTitle' => 'Corals',
				'id' => 'CoralsSpeciesRichness', 
				'type' => 'raster', 
				'scheme' => 'tms',
				'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:corals_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
				'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
	Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => 'Low richness',
					],[
						'color' => '#feedaa', 
						'label' => '',
					],[
						'color' => '#2b83ba', 
						'label' => 'High richness',
					]
				],
			]
		]],
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],

	'SpeciesList' => [
		'name' => 'Species list per protected area',
		'description' => 'The following species list is computed from the species ranges recorded in the IUCN Red List of Threatened Species.',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE,
		'layer' => FALSE, 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species'
    ],

	// 'SpeciesGbif' => [
	// 	'name' => 'Species occurrences reported to the GBIF',
	// 	'description' => 'Species occurrences map reported to the Global Biodiversity Information Facility (GBIF).', 
	// 	'buttonGroup' => NULL,
	// 	'chart' => FALSE,
	// 	'table' => FALSE, 
	// 	'layer' => NULL,
	// 	'customBottom' => '<div id="wrapper-gbif-ata"></div>', //todo - everything will be added here via JS...
	// 	'link' => 'https://www.gbif.org/', 
	// 	'linkText' => "GBIF Home Page", 
    // ]



	// 	'SpeciesAnimalPlantNumbers' => [
	// 		'name' => 'Reported number of animal and plant species',
	// 		'description' => 'The number of animal and plant species as reported by the IUCN Red List of Threatened Species',
	// 		'buttonGroup' => NULL,
	// 		'chart' => FALSE,
	// 		'table' => TRUE, 
	// 		'layer' => NULL,
	// 		'customBottom' => '', //todo 
	// 		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
	// 		'linkText' => NULL, 
	//     ],

	// 	'SpeciesThreatNumbers' => [
	// 		'name' => 'Reported number of threatened amphibians, birds and mammals',
	// 		'description' => 'The number of threatened, endemic and assessed amphibian, bird and mammals species as reported by the IUCN Red List of Threatened Species',
	// 		'buttonGroup' => NULL,
	// 		'chart' => FALSE,
	// 		'table' => TRUE, 
	// 		'layer' => NULL,
	// 		'customBottom' => '', //todo 
	// 		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
	// 		'linkText' => NULL, 
	//     ],
	// 	'SpeciesThreatEndemicNumbers' => [
	// 		'name' => 'Reported endemic and threatened endemic vertebrates in the country',
	// 		'description' => 'The number of threatened and endemic vertebrates as reported by the IUCN Red List of Threatened Species',
	// 		'buttonGroup' => NULL,
	// 		'chart' => FALSE,
	// 		'table' => TRUE, 
	// 		'layer' => NULL,
	// 		'customBottom' => '', //todo 
	// 		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
	// 		'linkText' => NULL, 
	//     ], 
];

$DataCardsTourism = [];

$DataCardsMarineAndCoastal = [];
?>