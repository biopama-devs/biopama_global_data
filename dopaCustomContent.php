<?php
ob_start();
?> 

<div class="card bg-gray-1200 mb-3">
  <div class="card-body">
	<h6 class="card-title">Country environment and development profiles</h6>
	<div class="card-text">
	  Useful external links to Country Environment and Development Profiles.
	  <br>
	  <hr>
	  <div id="wrapper-links" style="display: block;"> 
		<div class="links-group">
		  <h5>Country statistics</h5>
			<ul class="profile-links">
				<li><a class="token-link" href="http://data.un.org/en/iso/[ISO2].html" target="_blank"> UN Data</a> </li>
				<li><a class="token-link" href="https://databank.worldbank.org/views/reports/reportwidget.aspx?Report_Name=CountryProfile&Id=b450fd57&tbar=y&dd=y&inf=n&zm=n&country=[ISO3]" target="_blank"> World Bank  </a> </li>
				<li><a class="token-link" href="https://www.fao.org/countryprofiles/index/en/?iso3=[ISO3]" target="_blank"> FAO country statistics </a> </li>
			</ul>
		</div>
		<div class="links-group">
		  <h5>Development indicators</h5>
			<ul class="profile-links">
				<li><a class="token-link" href="https://hdr.undp.org/en/countries/profiles/[ISO3]" target="_blank"> UNDP Country Profile</a> </li>
			</ul>
		</div>
		<div class="links-group">
		  <h5>Environment and biodiversity indicators</h5>
			<ul class="profile-links">
				<li><a class="token-link" href="https://www.cbd.int/countries/?country=[ISO2]" target="_blank"> UN Convention on Biological Diversity (CBD) country profile</a> </li>
				<li><a class="token-link" href="http://datazone.birdlife.org/country/[COUNTRYNAME]" target="_blank"> Country profile by BirdLife International </a> </li>
				<li><a class="token-link" href="http://www.gbif.org/country/[ISO2]" target="_blank"> Global Biodiversity Information Facility (GBIF) country report</a> </li>
			</ul>
		</div>
		<div class="links-group">
		  <h5>Protected areas</h5>
			<ul class="profile-links">
				<li><a class="token-link" href="https://www.cbd.int/protected/implementation/actionplans/country/?country=[ISO2]" target="_blank"> CBD Programme of Work on Protected Areas (PoWPA) Action Plan </a> </li>
				<li><a class="token-link" href="https://www.protectedplanet.net/country/[ISO2]" target="_blank"> UNEP-WCMC Country Profile from the World Database on Protected Areas (WDPA) </a> </li>
				<li><a class="token-link" href="https://www.ecolex.org/result/?q=&amp;type=legislation&amp;xkeywords=protected+area&amp;xcountry=[COUNTRYNAME]" target="_blank"> ECOLEX: Country environmental legislation </a> </li>
			</ul>
		</div>
	  </div>
	</div>
  </div>
</div> 

<?php 
	$ProfileCustom = ob_get_clean();
	$customCards->custom = $ProfileCustom;
?>